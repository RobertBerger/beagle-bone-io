#!/bin/bash

echo "Compiling the overlay from .dts to .dtbo"

echo "+ ../dtc/dtc -O dtb -o DM-GPIO-Test-00A0.dtbo -b 0 -@ DM-GPIO-Test.dts"
../dtc/dtc -O dtb -o DM-GPIO-Test-00A0.dtbo -b 0 -@ DM-GPIO-Test.dts
