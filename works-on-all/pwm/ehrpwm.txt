see also: 

http://processors.wiki.ti.com/index.php/AM335x_PWM_Driver%27s_Guide

Changing PWM frequency at run time: 
https://groups.google.com/forum/#!topic/beagleboard/a___MDZ5vks

cat /sys/kernel/debug/pwm

platform/48304200.ehrpwm, 2 PWM devices
 pwm-0   ((null)              ):
 pwm-1   ((null)              ):

platform/48302200.ehrpwm, 2 PWM devices
 pwm-0   ((null)              ):
 pwm-1   ((null)              ):

platform/48300100.ecap, 1 PWM device
 pwm-0   ((null)              ):

ls /sys/class/pwm
pwmchip0  pwmchip1  pwmchip3

P9_14, P9_16:

fdt:

        ehrpwm1_pin_p9_14: pinmux_ehrpwm1_pin_p9_14 {
                pinctrl-single,pins = <
                        0x048 0x6       /* P9_14 (ZCZ ball U14) | MODE 6 */
                >;
        };

        ehrpwm1_pin_p9_16: pinmux_ehrpwm1_pin_p9_16 {
                pinctrl-single,pins = <
                        0x04c 0x6       /* P9_16 (ZCZ ball T14) | MODE 6 */
                >;
        };



&epwmss1 {
        pinctrl-names = "default";
        pinctrl-0 = <
                &ehrpwm1_pin_p9_14
                &ehrpwm1_pin_p9_16
        >;

        status = "okay";

        ehrpwm@48302200 {
                status = "okay";
        };
};

-----------

echo 0 > /sys/class/pwm/pwmchip1/export

cd /sys/class/pwm/pwmchip1/pwm0

1 kHz: 
  period: 1mS 
  duty cycle: 0.5mS

echo 0 > enable
in nsec:
echo 1000000 > period
in nsec:
echo 500000 > duty_cycle
echo 1 > enable


full bright:

echo 0 > enable
echo 100000000 > period
echo 100000000 > duty_cycle
echo 1 > enable

dimmer:

echo 0 > enable
echo 100000000 > period
echo 20000000 > duty_cycle
echo 1 > enable

echo 0 > enable
echo 50000000 > period
echo 50000000 > duty_cycle
echo 1 > enable

200 Hz (OK for LCD?)

echo 0 > enable
echo 20000000 > period
echo 10000000 > duty_cycle
echo 1 > enable


echo 0 > enable
echo 20000000 > period
echo 5000000 > duty_cycle
echo 1 > enable


100 Hz:

bright

echo 0 > enable
echo 10000000 > period
echo 10000000 > duty_cycle
echo 1 > enable

dimmer light:

echo 0 > enable
echo 10000000 > period
echo 5000000 > duty_cycle
echo 1 > enable

dimm light:

echo 0 > enable
echo 10000000 > period
echo 1000000 > duty_cycle
echo 1 > enable

off:

echo 0 > enable


-----------

echo 1 > /sys/class/pwm/pwmchip1/export

cd /sys/class/pwm/pwmchip1/pwm1

1 kHz:
  period: 1mS
  duty cycle: 0.5mS

does not like echo 1000000 > period
-sh: echo: write error: Invalid argument

100 Hz:

echo 0 > enable
echo 10000000 > period
echo 5000000 > duty_cycle
echo 1 > enable

---------

echo 0 > /sys/class/pwm/pwmchip3/export

cd /sys/class/pwm/pwmchip3/pwm0


echo 0 > enable
echo 20000000 > period
echo 20000000 > duty_cycle
echo 1 > enable

echo 0 > enable
echo 20000000 > period
echo 10000000 > duty_cycle
echo 1 > enable

echo 0 > enable
echo 20000000 > period
echo 0 > duty_cycle
echo 1 > enable






-------

echo 1 > /sys/class/pwm/pwmchip3/export

cd /sys/class/pwm/pwmchip3/pwm1




=============
Always set duty_cycle to zero BEFORE you set the frequency if the previous value will not fit into the new period, this avoids frustration.

see also: 

http://processors.wiki.ti.com/index.php/AM335x_PWM_Driver%27s_Guide
http://processors.wiki.ti.com/index.php/Linux_Core_PWM_User%27s_Guide
https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/arch/arm/boot/dts/am33xx.dtsi?id=refs/tags/v3.16-rc7


P9_14, P9_16: (EHRPWM1A, EHRPWM1B)

fdt:


        ehrpwm1_pin_p9_14: pinmux_ehrpwm1_pin_p9_14 {
                pinctrl-single,pins = <
                        0x048 0x6       /* P9_14 (ZCZ ball U14) | MODE 6 */
                >;
        };

        ehrpwm1_pin_p9_16: pinmux_ehrpwm1_pin_p9_16 {
                pinctrl-single,pins = <
                        0x04c 0x6       /* P9_16 (ZCZ ball T14) | MODE 6 */
                >;
        };

&epwmss1 {
        pinctrl-names = "default";
        pinctrl-0 = <
                &ehrpwm1_pin_p9_14
                &ehrpwm1_pin_p9_16
        >;

        status = "okay";

        ehrpwm@48302200 {
                status = "okay";
        };
};

--->

try this as well:

       ehrpwm2_pin_p8_19: pinmux_ehrpwm2_pin_p8_19 {
                 pinctrl-single,pins = <
                        0x0020 0x04    /* P8_19 | MODE 4 */
       };


       ehrpwm2_pin_p8_13: pinmux_ehrpwm2_pin_p8_13 {
                 pinctrl-single,pins = <
                        0x024 0x4       /* P8_13 | MODE 4 */
                 >;
       };


&epwmss2 {
        pinctrl-names = "default";
        pinctrl-0 = <
                &ehrpwm2_pin_p8_19
                &ehrpwm2_pin_p8_13
        >;

        status = "okay";

        ehrpwm@48304200 {
                status = "okay";
        };


<--

-----------

Always set duty_cycle to zero BEFORE you set the frequency if the previous value will not fit into the new period, this avoids frustration!!!
For the sake of completeness: I measured the smallest possible pulse step size to be 10 ns. Furthermore, the pulse width timer has a 16 Bit resolution. So be aware of rounding problems.



echo 0 > /sys/class/pwm/pwmchip1/export

cd /sys/class/pwm/pwmchip1/pwm0

1 kHz: 
  period: 1mS 
  duty cycle: 0.5mS

echo 0 > enable
in nsec:
echo 1000000 > period
in nsec:
echo 500000 > duty_cycle
echo 1 > enable

100 Hz:

echo 0 > enable
echo 10000000 > period
echo 5000000 > duty_cycle
echo 1 > enable

-----------

echo 1 > /sys/class/pwm/pwmchip1/export

cd /sys/class/pwm/pwmchip1/pwm1

1 kHz:
  period: 1mS
  duty cycle: 0.5mS

does not like echo 1000000 > period
-sh: echo: write error: Invalid argument

100 Hz:

echo 0 > enable
echo 10000000 > period
echo 5000000 > duty_cycle
echo 1 > enable

---------
