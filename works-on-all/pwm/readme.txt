How do you generate the PWM output (which hardware subsystems do you use)? And how do you controll them (by which software)?

When you use the PWMSS subsystems (either eCAP or PWM modules), you can synchronize two subsystems by setting some bits in the control registers. See SRM chapter 15 for details. Ie. you can use libpruio for easy access to those registers.

SRM: http://www.ti.com/lit/ug/spruh73l/spruh73l.pdf
http://beagleboard.org/project/libpruio/


