IIO pulse capture support for TI ECAP: lost?
https://lkml.org/lkml/2014/2/5/378


see also: 

http://processors.wiki.ti.com/index.php/AM335x_PWM_Driver%27s_Guide

P9_42:

fdt:

        ecap0_pin_p9_42: pinmux_ecap0_pin_p9_42 {
                pinctrl-single,pins = <
                        0x164 0x0       /* P9_42 (ZCZ ball C18) | MODE 0 */
                >;
        };


&epwmss0 {
        pinctrl-names = "default";
        pinctrl-0 = <&ecap0_pin_p9_42>;
        status = "okay";

        ecap@48300100 {
                status = "okay";
        };
};

echo 0 > /sys/class/pwm/pwmchip0/export

cd /sys/class/pwm/pwmchip0/pwm0

1 kHz: 
  period: 1mS 
  duty cycle: 0.5mS

echo 0 > enable
in nsec:
echo 1000000 > period
in nsec:
echo 500000 > duty_cycle
echo 1 > enable

100 Hz:

echo 0 > enable
echo 10000000 > period
echo 5000000 > duty_cycle
echo 1 > enable

