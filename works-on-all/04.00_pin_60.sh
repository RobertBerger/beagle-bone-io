#Since each GPIO module provides 32 dedicated GPIOs (general purpose input/output) 
# and the GPIOs support 4 banks of 32 GPIOs (so, 128 GPIOs in total) the naming of 
# GPIO0_5, would be GPIO 5 as (0 x32 + 5 = 5)
#
#(Pin  12 on the P9 Header) GPIO1_28 – The LED  = 1 x 32 + 28 = GPIO 60 (Offset 0×078, P9-12 GPIO1_28) #88
#
#NOTE: GPIO 60 is not PIN 60!!!
#
#If we check pins again and search for pin 60, by using the offset we can see:
#
echo "+ more $PINS |grep 878"
more $PINS |grep 878

echo "37 means 110111 = Fast, Enable Receiver, Pullup type, enabled, mux mode 7."

#
#Well if you look at this table, you see that 0×30 means the slew rate is fast,
#the receiver is enabled, the pad is set for pullup and pullup is enabled. 
#The Mode is 0. which means when you look a the table for this pin on the P9 header (Table 8. Expansion Header P9 Pinout), you see that pin is set as:
#
# Beaglebone P9 Header, Pin 12, Mode 0 is gpmc_be1n, we would like to set it to Mode 7, which is gpio1[28] (Note the LED is currently on).
#
# 27 means 100111 = Fast, Enable Receiver, Pulldown type, enabled, mux mode 7.
# 
# 37 means 110111 = Fast, Enable Receiver, Pullup type, enabled, mux mode 7.
#
# Be careful, not all pins work in this way and there are external resistors on the board that affect the behaviour. 
# For example, pins GPIO2_6 to GPIO2_14 all have external 42.2k resistors to GND and 100k resistors to high.


