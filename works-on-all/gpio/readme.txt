cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pingroups

stuff I added in the fdt:

pinmux_ehrpwm2_pin_p8_19 gpio Bank0.Pin22 PIN#22
pinmux_ehrpwm2_pin_p8_13 gpio Bank0.Pin23 PIN#23



stuff I did not add to the fdt:
               pin_p9_12 gpio Bank1.Pin28 PIN#60 

traffic_leds_s0    p9_11 gpio Bank0.Pin30 PIN#30 


...
group: pinmux_ehrpwm2_pin_p8_19
pin 8 (44e10820.0)

group: pinmux_ehrpwm2_pin_p8_13
pin 9 (44e10824.0)
...
group: pinmux_hd4470_pins
pin 30 (44e10878.0)
...

-----

cat /sys/kernel/debug/pinctrl/44e10800.pinmux/pinmux-pins

pin x (xxxxxxxxxx): (MUX UNCLAIMED) (GPIO UNCLAIMED)
...
pin 8 (44e10820.0): 48304000.epwmss (GPIO UNCLAIMED) function pinmux_ehrpwm2_pin_p8_19 group pinmux_ehrpwm2_pin_p8_19
pin 9 (44e10824.0): 48304000.epwmss (GPIO UNCLAIMED) function pinmux_ehrpwm2_pin_p8_13 group pinmux_ehrpwm2_pin_p8_13
...
pin 30 (44e10878.0): gpio_leds.8 (GPIO UNCLAIMED) function traffic_leds_s0 group traffic_leds_s0
...

-----

export PINS=/sys/kernel/debug/pinctrl/44e10800.pinmux/pins

cat $PINS

                     mode
...
pin 8 (44e10820.0) 00000004 pinctrl-single
pin 9 (44e10824.0) 00000004 pinctrl-single
...
pin 30 (44e10878.0) 00000007 pinctrl-single
...

-----

https://gist.github.com/ajmontag/4013192



===============================================



http://lwn.net/Articles/532714/
http://lwn.net/Articles/533632/

block gpio


gpio in kernel space:
http://processors.wiki.ti.com/index.php/GPIO_Driver_Guide

https://www.kernel.org/doc/Documentation/gpio/


Rasppi:
http://sysprogs.com/VisualKernel/tutorials/raspberry/leddriver/

---------------------
http://www.armhf.com/using-beaglebone-black-gpios/
http://www.circuidipity.com/bbb-led.html
----------------------
input:
http://hipstercircuits.com/capture-input-events-via-gpio-on-beaglebone-black/




---------------------
BBB:

http://www.linux.com/learn/tutorials/765810-beaglebone-black-how-to-get-interrupts-through-linux-gpio

-----------------------
some user space stuff:
https://github.com/VegetableAvenger/BBBIOlib

-----------------------
from kernel space:
GPIO in kernel: an introduction
http://lwn.net/Articles/532714/
GPIO in the kernel: future directions
http://lwn.net/Articles/533632/

Marek GPIO keyboard matrix:
http://localhost/lxr/http/source/linux/drivers/input/keyboard/matrix_keypad.c#L447

156                         input_event(input_dev, EV_MSC, MSC_SCAN, code);
157                         input_report_key(input_dev,
158                                          keycodes[code],
159                                          new_state[col] & (1 << row));
160                 }
161         }
162         input_sync(input_dev);


------


80 static char pm_name_powermate[] = "Griffin PowerMate";
81 static char pm_name_soundknob[] = "Griffin SoundKnob";
82 

109         /* handle updates to device state */
110         input_report_key(pm->input, BTN_0, pm->data[0] & 0x01);
111         input_report_rel(pm->input, REL_DIAL, pm->data[1]);
112         input_sync(pm->input);
------

 39 static void report_key_event(struct input_dev *input, int keycode)
 40 {
 41         /* simulate a press-n-release */
 42         input_report_key(input, keycode, 1);
 43         input_sync(input);
 44         input_report_key(input, keycode, 0);
 45         input_sync(input);
 46 }
 47 
 48 static void report_rotary_event(struct bfin_rot *rotary, int delta)
 49 {
 50         struct input_dev *input = rotary->input;
 51 
 52         if (rotary->up_key) {
 53                 report_key_event(input,
 54                                  delta > 0 ? rotary->up_key : rotary->down_key);
 55         } else {
 56                 input_report_rel(input, rotary->rel_code, delta);
 57                 input_sync(input);


------




lirc:
https://github.com/hani93/lirc_bbb
lirc hardware:
http://aron.ws/projects/lirc_rpi/
http://alexba.in/blog/2013/01/06/setting-up-lirc-on-the-raspberrypi/
http://wiki.openelec.tv/index.php?title=Guide_To_lirc_rpi_GPIO_Receiver

API changed: ??? old still working I think
gpio_direction_input(gpio_in_pin); 

#include <linux/gpio/consumer.h>

int gpiod_direction_input(struct gpio_desc *desc);

In short: the gpio_ prefix on the existing GPIO functions has been changed to gpiod_ and the integer GPIO number argument is now a struct gpio_desc *. There is also a new include file for the new functions; otherwise the interfaces are identical. The existing, integer-based API still exists, but it has been reimplemented as a layer on top of the descriptor-based API shown here. 

 What is missing from the above list, though, is any way of obtaining a descriptor for a GPIO line in the first place. One way to do that is to get the descriptor from the traditional GPIO number:

    struct gpio_desc *gpio_to_desc(unsigned gpio);






http://www.michaelhleonard.com/gpio-interface-access-conventions-on-linux/
http://mondi.web.cs.unibo.it/gpio_control.html
https://github.com/Jeshwanth/Linux_modules_examples/blob/master/beaglebone-GPIO_int/gpio_int.c
http://sergioprado.org/pin-control-device-tree-e-gpios-na-beaglebone-black/
https://translate.google.com/translate?sl=auto&tl=en&js=y&prev=_t&hl=en&ie=UTF-8&u=http%3A%2F%2Fsergioprado.org%2Fpin-control-device-tree-e-gpios-na-beaglebone-black%2F&edit-text=




-----------------------
drivers/leds/leds-gpio.c

devm_gpio_request
and 
gpio_request

drivers/gpio/gpio-omap.c:


1131 static const struct of_device_id omap_gpio_match[];

1145         match = of_match_device(of_match_ptr(omap_gpio_match), dev);

1171 #ifdef CONFIG_OF_GPIO
1172         bank->chip.of_node = of_node_get(node);
1173 #endif
1174         if (node) {
1175                 if (!of_property_read_bool(node, "ti,gpio-always-on"))
1176                         bank->loses_context = true;
1177         } else {
1178                 bank->loses_context = pdata->loses_context;
1179 
1180                 if (bank->loses_context)
1181                         bank->get_context_loss_count =
1182                                 pdata->get_context_loss_count;
1183         }

1620 static struct platform_driver omap_gpio_driver = {
1621         .probe          = omap_gpio_probe,
1622         .driver         = {
1623                 .name   = "omap_gpio",
1624                 .pm     = &gpio_pm_ops,
1625                 .of_match_table = of_match_ptr(omap_gpio_match),

1602 static const struct of_device_id omap_gpio_match[] = {
1603         {
1604                 .compatible = "ti,omap4-gpio",
1605                 .data = &omap4_pdata,
1606         },
1607         {
1608                 .compatible = "ti,omap3-gpio",
1609                 .data = &omap3_pdata,
1610         },
1611         {
1612                 .compatible = "ti,omap2-gpio",
1613                 .data = &omap2_pdata,
1614         },
1615         { },
1616 };
1617 MODULE_DEVICE_TABLE(of, omap_gpio_match);

1620 static struct platform_driver omap_gpio_driver = {
1621         .probe          = omap_gpio_probe,
1622         .driver         = {
1623                 .name   = "omap_gpio",
1624                 .pm     = &gpio_pm_ops,
1625                 .of_match_table = of_match_ptr(omap_gpio_match),
1626         },
1627 };
