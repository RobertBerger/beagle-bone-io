rm=/bin/rm -f

#CXX=${CROSS_COMPILE}g++
#CC=${CROSS_COMPILE}gcc
#LINKER=${CROSS_COMPILE}gcc
LINKER=$(CC)
#LOADER=${CROSS_COMPILE}ldd
#STRIP=${CROSS_COMPILE}strip
#FILE=file
# you can find great_code on http://gitorious.org/greatcode-for-gnu-linux
# .git clone git://gitorious.org/greatcode-for-gnu-linux/greatcode-for-gnu-linux.git
#GREAT_CODE=${HOME}/projects/EGLiSA_for_trainer/src/29_great_code/greatcode-for-gnu-linux/Sources/gcode-1.140-p1 -eol_unix-
#GREAT_CODE=great_code -cfg_name-$(MAKE_DIR)/Gc.cfg -eol_unix-
#GREAT_CODE=../29_great_code/greatcode-for-gnu-linux/Sources/gcode-1.140-p1 -eol_unix-
GREAT_CODE=echo
#GREAT_CODE=/opt/gcode/gcode-1.140-p1 -eol_unix-
INDENT=indent -nbad -bap -nbc -bbo -hnl -br -brs -c33 -cd33 -ncdb -ce -ci4 -cli0 -d0 -di1 -nfc1 -i8 -ip0 -l80 -lp -npcs -nprs -npsl -sai -saf -saw -ncs -nsc -sob -nfca -cp33 -ss -ts8 -il1

DEFS+=  
EXE= $(PROJECT_NAME).out
INCLUDES+=  -I. -I../../include/
LIBS+=

DEFINES+= $(INCLUDES) $(DEFS) 
#CFLAGS+= $(DEFINES) -Wall -pedantic
CFLAGS+= $(DEFINES) -Wall -Wextra -Wshadow -Wstrict-aliasing -ansi -pedantic -Werror
LDFLAGS+= 


OBJS = $(OTHER_OBJS) $(PROJECT_NAME).o

.c.o:
	$(rm) $@
	$(CC) $(CFLAGS) -c $*.c
	@$(GREAT_CODE) -file-$*.c
	@$(INDENT) $*.c
	@rm -f $*.c~

.cpp.o:
	$(rm) $@
	$(CXX) $(CXXFLAGS) -c $*.cpp
	@$(GREAT_CODE) -file-$*.cpp
	@$(INDENT) $*.cpp
	@rm -f $*.cpp~

all: $(EXE)

$(EXE) : $(OBJS)
	$(LINKER) $(OBJS) $(LDFLAGS) -o $(EXE) $(LIBS)

clean:
	$(rm) $(OBJS) $(EXE) core.* core *~ gmon.out *.gcov *.gcda *.gcno vgcore.*

libs_used:
	$(LOADER) $(EXE)

file_type:
	$(FILE) $(EXE)
strip:
	$(STRIP) $(EXE) -o $(EXE).stripped

