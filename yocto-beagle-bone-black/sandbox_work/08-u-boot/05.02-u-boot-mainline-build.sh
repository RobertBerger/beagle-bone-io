source ../../env.sh

HERE=$(pwd)

            # --> hack for ELDK 5.3
            # original: -Wl,-O1 -Wl,--hash-style=gnu -Wl,--as-needed
            #           -Wl,-01,... is not recognized by linker
            echo "LDFLAGS:"
            export LDFLAGS=""
            export | grep LD_FLAGS
            # <-- hack for ELDK 5.3

EXTRAVER="mainline-${USER}-${UBOOT_MAINLINE_VER}"

echo "+ rm -rf  ${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}"
rm -rf  ${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}

# buidling in BUILD_DIR is broken at least since v2012.10 
# No from 2013.07 you just need to add ${BUILD_DIR} to the build target
# from 2014.04 BUILD_DIR does not work like before - kbuild is used now
echo "+ mkdir -p ${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}"
mkdir -p ${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}
echo "+ export BUILD_DIR=${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}"
export BUILD_DIR=${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}

echo "+ cd ${HOME}/${TARGET_BOARD}/u-boot"
cd ${HOME}/${TARGET_BOARD}/u-boot

echo "+ make mrproper"
make mrproper

echo "+ make O=${BUILD_DIR} mrproper"
make O=${BUILD_DIR} mrproper

echo "+ find configs | grep bone"
find configs | grep bone

echo "+ make O=${BUILD_DIR} ${UBOOT_CONFIG}"
make O=${BUILD_DIR} ${UBOOT_CONFIG}

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make menuconfig, but now I make make oldconfig"
  echo "+ make O=${BUILD_DIR} oldconfig"
  make O=${BUILD_DIR} oldconfig
else
  echo "+ make O=${BUILD_DIR} menuconfig"
  make O=${BUILD_DIR} menuconfig
fi

echo "+ make -j $(getconf _NPROCESSORS_ONLN) O=${BUILD_DIR} EXTRAVERSION=-${EXTRAVER} all"
make -j $(getconf _NPROCESSORS_ONLN) O=${BUILD_DIR} EXTRAVERSION=-${EXTRAVER} all

echo "+ strings $BUILD_DIR/u-boot.bin | grep \"U-Boot ${UBOOT_MAINLINE_VER:1:${#UBOOT_MAINLINE_VER}}\""
strings $BUILD_DIR/u-boot.bin | grep "U-Boot ${UBOOT_MAINLINE_VER:1:${#UBOOT_MAINLINE_VER}}"

cd ${HERE}
