source ../../env.sh

HERE=$(pwd)

if [ ! -d ${HOME}/${TARGET_BOARD} ];
then
    mkdir -p ${HOME}/${TARGET_BOARD}
else
    echo "--> ${HOME}/${TARGET_BOARD} exists, not creating it <--"
fi

cd ${HOME}/${TARGET_BOARD}

if [ ! -d u-boot ];
then
  echo "+ let's try with gitpod first"
  echo "+ git clone gp@gitpod:u-boot.git"
  git clone gp@gitpod:u-boot.git
      if [[ $? != 0 ]]; then
        echo "+ we don't have gitpot - need to go out on the web"
        echo "+ git clone git://git.denx.de/u-boot.git"
        git clone git://git.denx.de/u-boot.git
      fi
else
    echo "--> ${HOME}/${TARGET_BOARD}/u-boot exists, not creating it <--"
fi

cd ${HERE}
