source ../../env.sh

HERE=$(pwd)

EXTRAVER="mainline-${USER}-${UBOOT_MAINLINE_VER}"

export BUILD_DIR=${HOME}/${TARGET_BOARD}/u-boot-build-${EXTRAVER}

echo "+ strings $BUILD_DIR/u-boot.img | grep \"U-Boot ${UBOOT_MAINLINE_VER:1:${#UBOOT_MAINLINE_VER}}\""
strings $BUILD_DIR/u-boot.img | grep "U-Boot ${UBOOT_MAINLINE_VER:1:${#UBOOT_MAINLINE_VER}}"

echo "+ enter the IP address of the board"
read ipaddr

echo ${ipaddr}

echo "+ check on the board that SD card is mounted"
echo "+ if not: /etc/init.d/udev restart on the board"
echo "+ execute the following commands:"
echo "scp ${BUILD_DIR}/MLO root@${ipaddr}:/run/media/mmcblk0p1"
echo "scp ${BUILD_DIR}/MLO root@${ipaddr}:/run/media/mmcblk0p1/MLO-${UBOOT_MAINLINE_VER}"
echo "scp ${BUILD_DIR}/u-boot.img root@${ipaddr}:/run/media/mmcblk0p1"
echo "scp ${BUILD_DIR}/u-boot.img root@${ipaddr}:/run/media/mmcblk0p1/u-boot.img-${UBOOT_MAINLINE_VER}"
echo "+ reboot the board"

cd ${HERE}
