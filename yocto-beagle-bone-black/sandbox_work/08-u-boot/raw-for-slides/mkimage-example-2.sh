...
UIMAGE_LOADADDR ?= arch_must_set_this
UIMAGE_ENTRYADDR ?= $(UIMAGE_LOADADDR)
...
quiet_cmd_uimage = UIMAGE  $(UIMAGE_OUT)
cmd_uimage = $(CONFIG_SHELL) $(MKIMAGE) -A $(UIMAGE_ARCH) -O linux \
             -C $(UIMAGE_COMPRESSION) $(UIMAGE_OPTS-y) \
             -T $(UIMAGE_TYPE) \
             -a $(UIMAGE_LOADADDR) -e $(UIMAGE_ENTRYADDR) \
             -n $(UIMAGE_NAME) -d $(UIMAGE_IN) $(UIMAGE_OUT)
...
