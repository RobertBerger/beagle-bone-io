(*-->source ~/<TRAINING_ID>/<TOOLKIT>-<TARGET_BOARD>/<TRAINING_ID_SHORT>_for_student/env.sh<--*)
export EXTRAVER="mainline-${USER}-<UBOOT_MAINLINE_VER>"
export BUILD_DIR=${HOME}/<TARGET_BOARD>/u-boot-build-${EXTRAVER}
sudo mount /mnt/boot
sudo cp ${BUILD_DIR}/MLO /mnt/boot/
sudo cp ${BUILD_DIR}/u-boot.img /mnt/boot/
sync
sudo umount /mnt/boot
