LOADADDR is typically the address where the SOC starts searching for RAM + 32K (workspace for kernel decompressor) e.g. <SOC_RAM_START> + 0x8000 = <LOAD_ADDR_HARDCODED>.

In u-boot you define an address where uImage is being loaded to in RAM e.g. kernel_addr_r=<KERNEL_ADDR_R_HARDCODED>.

When you compile the kernel you need to pass something like LOADADDR=<LOAD_ADDR_HARDCODED> which ends up in kbuild (see previous slide) as -a $(UIMAGE_LOADADDR) (expands to -a <UBOOT_LOADADDRESS>) and -e $(UIMAGE_ENTRYADDR) (expands to -e <UBOOT_ENTRYPOINT>).

So u-boot will load uImage, say from tftp, to <KERNEL_ADDR_R_HARDCODED>, copy the playload to <UBOOT_LOADADDRESS> and jump to <UBOOT_ENTRYPOINT>.
