(*-->source ~/<TRAINING_ID>/<TOOLKIT>-<TARGET_BOARD>/<TRAINING_ID_SHORT>_for_student/env.sh<--*)
cd ${HOME}/<TARGET_BOARD>/u-boot
git checkout master
git tag -l
git branch -D <UBOOT_MAINLINE_VER>_LOCAL
git checkout <UBOOT_MAINLINE_VER>
git checkout -b <UBOOT_MAINLINE_VER>_LOCAL
