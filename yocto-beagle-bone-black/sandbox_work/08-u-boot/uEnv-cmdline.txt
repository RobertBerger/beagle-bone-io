setenv hostname beagle-bone-black

#setenv kernel_addr_r 0x80300000
setenv kernel_addr_r 0x80007fe0
setenv expand_bootfile 'setenv bootfile ${hostname}/uImage'

---

setenv fdt_addr_r 0x815f0000
setenv expand_fdtfile 'setenv fdtfile ${hostname}/uImage-am335x-boneblack.dtb'

---

setenv ipaddr 192.168.2.64
setenv serverip 192.168.2.217
setenv gatewayip 192.168.2.1
setenv netmask 255.255.255.0
setenv netdev eth0

---

setenv kernel_netload 'tftp ${kernel_addr_r} ${bootfile}'
setenv fdt_netload 'tftp ${fdt_addr_r} ${fdtfile}'
setenv netload 'run kernel_netload fdt_netload'

---

setenv bootargs console=ttyO0,115200n8
setenv nfsroot /opt/poky/1.7.1/core-image-sato-sdk-beaglebone

---

setenv ips_to_bootargs 'setenv bootargs ${bootargs} ip=${ipaddr}:${serverip}:${gatewayip}:${netmask}:${hostname}:${netdev}:off'

---

setenv nfs_to_bootargs 'setenv bootargs ${bootargs} nfsroot=${serverip}:${nfsroot},v3,tcp'
setenv default_to_bootargs 'setenv bootargs ${bootargs} noinitrd nohlt panic=1'

---

setenv compose_nfs_bootargs 'run ips_to_bootargs; run nfs_to_bootargs; run default_to_bootargs'
setenv nfsboot 'echo Booting from nfs...; run compose_nfs_bootargs'

---

setenv compose_nfsboot 'run nfsboot'

---

setenv compose_default 'run expand_bootfile; echo bootfile: ${bootfile}; run expand_fdtfile; echo fdtfile: ${fdtfile}; ping ${serverip}'

---

setenv uenvcmd 'run compose_default; run netload; run compose_nfsboot; printe bootargs; bootm ${kernel_addr_r} - ${fdt_addr_r}'
run uenvcmd

---------

run compose_default
run netload
run compose_nfsboot
printe bootargs
bootm ${kernel_addr_r} - ${fdt_addr_r}

-------



