source ../../env.sh

HERE=$(pwd)

if [ ! -d "${HOME}/${TARGET_BOARD}/u-boot" ]; then
  echo "+ FATAL: ${HOME}/${TARGET_BOARD}/u-boot does not exist."
  exit
fi

echo "+ cd ${HOME}/${TARGET_BOARD}/u-boot"
cd ${HOME}/${TARGET_BOARD}/u-boot

#clean up 
echo "+ git reset --hard origin"
git reset --hard origin
echo "+ git clean -f -d"
git clean -f -d

echo "+ git checkout master"
git checkout master
echo "+ git branch -D ${UBOOT_MAINLINE_VER}_LOCAL"
git branch -D ${UBOOT_MAINLINE_VER}_LOCAL

if [[ $INTERNET != *no* ]]; then
  echo "+ git pull"
  git pull
else
  echo "+ no INTERNET"
  echo "+ no git pull"
fi

echo "+ git checkout ${UBOOT_MAINLINE_VER}"
git checkout ${UBOOT_MAINLINE_VER}
echo "+ git checkout -b ${UBOOT_MAINLINE_VER}_LOCAL"
git checkout -b ${UBOOT_MAINLINE_VER}_LOCAL

cd ${HERE}
