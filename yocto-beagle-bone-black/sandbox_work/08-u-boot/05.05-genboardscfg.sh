#sudo apt-get install python-pip
#sudo apt-get install python-dev
#sudo pip install pyutilib.subprocess
#sudo pip install multiprocessing

# ELDK/Yocto sets up some funny Python stuff, 
# which keeps my test cases from running
# This is a work around which seems to work
(
source ../../env.sh

echo "echo \"+ cd ${HOME}/${TARGET_BOARD}/u-boot"\" > runme.sh
echo "cd ${HOME}/${TARGET_BOARD}/u-boot" >> runme.sh
echo "echo \"+ tools/genboardscfg.py"\" >> runme.sh
echo "tools/genboardscfg.py" >> runme.sh
chmod +x runme.sh
)
./runme.sh
rm -f runme.sh

