source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

echo "+ make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" ${FDT_NAME} EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}"

time make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" ${FDT_NAME} EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}

cd ${HERE}
