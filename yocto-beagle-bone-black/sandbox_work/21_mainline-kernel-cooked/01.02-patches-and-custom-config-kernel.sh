source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR} 

if [ -f ${HERE}/${KERNEL_MAINLINE_VER}/do-patches.sh ]; then
  echo "+ source ${HERE}/${KERNEL_MAINLINE_VER}/do-patches.sh"
  source ${HERE}/${KERNEL_MAINLINE_VER}/do-patches.sh
else
  echo "+ no patches to be applied"
fi

if [ -f ${HERE}/${KERNEL_MAINLINE_VER}/fix-config.sh ]; then
  echo "+ source ${HERE}/${KERNEL_MAINLINE_VER}/fix-config.sh"
  source ${HERE}/${KERNEL_MAINLINE_VER}/fix-config.sh
else
  echo "+ no cooked config (yet) - using ${KERNEL_DEFCONFIG}"
  echo "+ make ${KERNEL_DEFCONFIG}"
  make ${KERNEL_DEFCONFIG}
fi

cd ${HERE}
