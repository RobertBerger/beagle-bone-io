source ../../env.sh

#echo "going to checkout ${KERNEL_MAINLINE_VER} and to create a branch ${KERNEL_MAINLINE_VER}_LOCAL"

#if [[ $WORKSPACE = *jenkins* ]]; then
#  echo "WORKSPACE '$WORKSPACE' contains jenkins"
#  echo "would normally wait for input from command line"
#else
#  echo "press <ENTER> to go on"
#  read r
#fi

HERE=$(pwd)

if [ ! -d "${CUSTOM_KERNELDIR}" ]; then
  echo "+ FATAL: ${CUSTOM_KERNELDIR} does not exist."
  exit
fi

cd ${CUSTOM_KERNELDIR}

if [ -f ${HERE}/${KERNEL_MAINLINE_VER}/undo-patches.sh ];
then
   echo "+ source ${HERE}/${KERNEL_MAINLINE_VER}/undo-patches.sh"
   source ${HERE}/${KERNEL_MAINLINE_VER}/undo-patches.sh
fi

#clean up 
#echo "+ git reset --hard origin"
#git reset --hard origin
#echo "+ git clean -f -d"
#git clean -f -d

#echo "+ git checkout master"
#git checkout master
#echo "+ git branch -D ${KERNEL_MAINLINE_VER}_LOCAL"
#git branch -D ${KERNEL_MAINLINE_VER}_LOCAL

#if [[ $INTERNET != *no* ]]; then
#  echo "+ git pull"
#  git pull
#else
#  echo "+ no INTERNET"
#  echo "+ no git pull"
#fi
#
#echo "+ git checkout ${KERNEL_MAINLINE_VER}"
#git checkout ${KERNEL_MAINLINE_VER}
#echo "+ git checkout -b ${KERNEL_MAINLINE_VER}_LOCAL"
#git checkout -b ${KERNEL_MAINLINE_VER}_LOCAL
#cd ${HERE}
