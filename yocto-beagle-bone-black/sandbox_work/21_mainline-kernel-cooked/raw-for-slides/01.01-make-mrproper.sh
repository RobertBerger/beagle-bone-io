(source env.sh)
----
if these variables are not exported, export them:
HOSTCC=<HOSTCC>
HOSTCXX=<HOSTCXX>
CROSS_COMPILE=<CROSS_COMPILE>
CC=<CC>
----
cd /${HOME}/<TARGET_BOARD>/<CUSTOM_KERNEL_NAME>
make HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" mrproper
