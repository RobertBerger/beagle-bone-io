(source env.sh)
cd ${HOME}/<TARGET_BOARD>/<CUSTOM_KERNEL_NAME>
git branch
git checkout master
git tag -l
(optional) git branch -D <KERNEL_MAINLINE_VER>_LOCAL
git checkout <KERNEL_MAINLINE_VER>
git checkout -b <KERNEL_MAINLINE_VER>_LOCAL
