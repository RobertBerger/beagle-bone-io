source env.sh
----
TARGET_BOARD=<TARGET_BOARD>
ARCH=<ARCH>
----
sudo mount /mnt/beag2
sudo cp ${HOME}/${TARGET_BOARD}/<CUSTOM_KERNEL_NAME>/arch/${ARCH}/boot/uImage /mnt/beag2/boot/
ls -la /mnt/beag2/boot/
sudo sync
sudo umount /mnt/beag2
