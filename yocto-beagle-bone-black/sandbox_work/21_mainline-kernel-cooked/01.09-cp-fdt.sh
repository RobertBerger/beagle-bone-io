source ../../env.sh

HERE=$(pwd)

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally wait here, but not with jenkins"
else
  echo "do you really want to copy uImage with the version above to /opt/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/ ?"
  echo "press <ENTER> to go on"
  read r
fi

# last file in list will get the symlink
#for FDT in ${FDT_NAME} am335x-boneblack-res-1.dtb am335x-boneblack-res-2.dtb am335x-boneblack-res-3.dtb am335x-boneblack-res-4.dtb
for FDT in ${FDT_NAME} am335x-boneblack-res-1.dtb am335x-boneblack-res-2.dtb am335x-boneblack-res-3.dtb
do
  echo " ---> ${FDT}"
  echo "+ cp  ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT} ${TFTP_PREFIX}/${TARGET_BOARD}/${FDT}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID}"
  cp  ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT} ${TFTP_PREFIX}/${TARGET_BOARD}/${FDT}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID}
  cd ${TFTP_PREFIX}/${TARGET_BOARD}
  echo "+ ln -sf ${FDT}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID} uImage-am335x-boneblack.dtb"
  ln -sf ${FDT}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID} uImage-am335x-boneblack.dtb
  echo " <-- ${FDT}"
done

  cd ${TFTP_PREFIX}/${TARGET_BOARD}
  echo "+ ls --color=auto -la ${TFTP_PREFIX}/${TARGET_BOARD}"
  ls --color=auto -la ${TFTP_PREFIX}/${TARGET_BOARD}

cd ${HERE}

