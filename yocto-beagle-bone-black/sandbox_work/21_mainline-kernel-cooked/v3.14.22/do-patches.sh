echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch

# patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch depends on 
# patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch

# rber: this does not seem to apply - removed
#echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch"
#git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone/0001-arm-dts-am335x-bone-common-add-uart2_pins-uart4_pins.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone/0001-arm-dts-am335x-bone-common-add-uart2_pins-uart4_pins.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone-capes/0001-capes-ttyO1-ttyO2-ttyO4.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone-capes/0001-capes-ttyO1-ttyO2-ttyO4.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone-capes/0002-capes-Makefile.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts-bone-capes/0002-capes-Makefile.patch

# --> I doubt we need this cape supported here
# depends on patches/dts-bone-capes/0002-capes-Makefile.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/static-capes/0001-Added-Argus-UPS-cape-support.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/static-capes/0001-Added-Argus-UPS-cape-support.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/static-capes/0002-Added-Argus-UPS-cape-support-BBW.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/static-capes/0002-Added-Argus-UPS-cape-support-BBW.patch
# <-- I doubt we need this cape supported here

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0001-saucy-disable-Werror-pointer-sign.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0001-saucy-disable-Werror-pointer-sign.patch

# rber: this does not seem to apply - removed
#echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0002-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch"
#git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0002-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git diff"
  git diff
fi
