# patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch
# patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch
echo "+ git checkout arch/arm/boot/dts/am335x-boneblack.dts"
git checkout arch/arm/boot/dts/am335x-boneblack.dts

# patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch 
# depends on
# patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch depends on
echo "+ git checkout arch/arm/boot/dts/am335x-bone-common.dtsi"
git checkout arch/arm/boot/dts/am335x-bone-common.dtsi

# patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch
echo "+ git checkout drivers/pinctrl/pinctrl-single.c"
git checkout drivers/pinctrl/pinctrl-single.c

# patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch
echo "+ git checkout drivers/usb/musb/musb_host.c"
git checkout drivers/usb/musb/musb_host.c

# patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch
# patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch
echo "+ git checkout drivers/usb/musb/musb_cppi41.c"
git checkout drivers/usb/musb/musb_cppi41.c

# patches/dts-bone/0001-arm-dts-am335x-bone-common-add-uart2_pins-uart4_pins.patch
echo "+ git checkout arch/arm/boot/dts/am335x-bone-common.dtsi"
git checkout arch/arm/boot/dts/am335x-bone-common.dtsi

#  patches/dts-bone-capes/0001-capes-ttyO1-ttyO2-ttyO4.patch
echo "+ rm -f arch/arm/boot/dts/am335x-bone-ttyO1.dts"
rm -f arch/arm/boot/dts/am335x-bone-ttyO1.dts
echo "+ rm -f arch/arm/boot/dts/am335x-bone-ttyO2.dts"
rm -f arch/arm/boot/dts/am335x-bone-ttyO2.dts
echo "+ rm -f arch/arm/boot/dts/am335x-bone-ttyO4.dts"
rm -f arch/arm/boot/dts/am335x-bone-ttyO4.dts
echo "+ rm -f arch/arm/boot/dts/am335x-boneblack-ttyO1.dts"
rm -f arch/arm/boot/dts/am335x-boneblack-ttyO1.dts
echo "+ rm -f arch/arm/boot/dts/am335x-boneblack-ttyO2.dts"
rm -f arch/arm/boot/dts/am335x-boneblack-ttyO2.dts
echo "+ rm -f arch/arm/boot/dts/am335x-boneblack-ttyO4.dts"
rm -f arch/arm/boot/dts/am335x-boneblack-ttyO4.dts

# patches/dts-bone-capes/0002-capes-Makefile.patch
echo "+ git checkout arch/arm/boot/dts/Makefile"
git checkout arch/arm/boot/dts/Makefile

# --> I doubt we need this cape supported here 
# patches/static-capes/0001-Added-Argus-UPS-cape-support.patch
# depends on patches/dts-bone-capes/0002-capes-Makefile.patch
echo "+ git checkout drivers/misc/Kconfig"
git checkout drivers/misc/Kconfig
echo "+ git checkout drivers/misc/Makefile"
git checkout drivers/misc/Makefile
echo "+ rm -f arch/arm/boot/dts/am335x-boneblack-cape-bone-argus.dts"
rm -f arch/arm/boot/dts/am335x-boneblack-cape-bone-argus.dts
echo "+ rm -rf drivers/misc/cape_bone_argus"
rm -rf drivers/misc/cape_bone_argus
# patches/static-capes/0002-Added-Argus-UPS-cape-support-BBW.patch
echo "+ rm -f arch/arm/boot/dts/am335x-bone-cape-bone-argus.dts"
rm -f arch/arm/boot/dts/am335x-bone-cape-bone-argus.dts
# <-- I doubt we need this cape supported here 

# patches/saucy/0001-saucy-disable-Werror-pointer-sign.patch
echo "+ git checkout Makefile"
git checkout Makefile

# rber: does not apply - removed
# patches/saucy/0002-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch
# echo "+ git checkout drivers/gpu/drm/omapdrm/omap_plane.c"
# git checkout drivers/gpu/drm/omapdrm/omap_plane.c

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git status"
  git status
  echo "+ git diff"
  git diff
fi
