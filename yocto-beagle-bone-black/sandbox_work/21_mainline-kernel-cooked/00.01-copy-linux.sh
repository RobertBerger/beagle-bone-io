source ../../env.sh

HERE=$(pwd)

if [ ! -d ${HOME}/${TARGET_BOARD} ]; then
   echo "+ there is no ${HOME}/${TARGET_BOARD} folder"
   echo "+ mkdir -p ${HOME}/${TARGET_BOARD}"
   mkdir -p ${HOME}/${TARGET_BOARD}
fi


if [ -d ${CUSTOM_KERNELDIR} ]; then
  # we already have a CUSTOM_KERNELDIR
   echo "+ --> ${CUSTOM_KERNELDIR} already exists <--"
else
  # CUSTOM_KERNLDIR does not exist
  if [ -d ${KERNELDIR} ]; then
    # but we do have a (mainline) KERNELDIR
    echo "+ mkdir -p ${CUSTOM_KERNELDIR}"
    mkdir -p ${CUSTOM_KERNELDIR}
    # let's rsync it to CUSTOM_KERNELDIR
    echo "+ rsync -aP ${KERNELDIR}/ ${CUSTOM_KERNELDIR}"
    rsync -aP ${KERNELDIR}/ ${CUSTOM_KERNELDIR}
  else
   # we don't have CUSTOM_KERNELDIR and also no KERNELDIR
   # so we'll get it from git
   echo "+ cd ${HOME}/${TARGET_BOARD}"
   cd ${HOME}/${TARGET_BOARD}

  echo "+ let's try with gitpod first"
  echo "+ git clone ${GITPOD_KERNEL_GIT_REPO} ${CUSTOM_KERNEL_NAME}"
  git clone ${GITPOD_KERNEL_GIT_REPO} ${CUSTOM_KERNEL_NAME}
      if [[ $? != 0 ]]; then
        echo "+ we don't have gitpot - need to go out on the web"
        echo "+ git clone ${KERNEL_GIT_REPO} ${CUSTOM_KERNEL_NAME}"
        git clone ${KERNEL_GIT_REPO} ${CUSTOM_KERNEL_NAME}
      fi
  fi
fi

cd ${HERE}
