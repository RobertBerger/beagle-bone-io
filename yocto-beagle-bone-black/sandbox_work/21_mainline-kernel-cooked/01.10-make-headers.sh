source ../../env.sh

#EXTRAVER="-default"

HERE=(`pwd`)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

#echo "+ make help | grep headers"
#make help | grep headers

# headers_check   - Sanity check on exported header
#echo "+ make headers_check"
#make headers_check

# headerdep       - Detect inclusion cycles in headers
#echo "+ make headerdep"
#make headerdep 

# headers_install - Install sanitised kernel headers to INSTALL_HDR_PATH

echo "+ sudo make headers_install ARCH=${ARCH} INSTALL_HDR_PATH=/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo make headers_install ARCH=${ARCH} INSTALL_HDR_PATH=/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

cd ${HERE}

