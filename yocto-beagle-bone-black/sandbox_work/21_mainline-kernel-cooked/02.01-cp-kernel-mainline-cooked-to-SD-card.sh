source ../../env.sh

HERE=(`pwd`)

echo "Linux version:"
strings ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/Image | grep "Linux version"

echo "do you really want to copy ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage with the version above to /mnt/beag3/boot/?"
echo "press <ENTER> to go on"
read r

sudo mount /mnt/beag3

sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage /mnt/beag3/boot/uImage

ls -la /mnt/beag3/boot/

sudo sync
sudo umount /mnt/beag3

cd ${HERE}
