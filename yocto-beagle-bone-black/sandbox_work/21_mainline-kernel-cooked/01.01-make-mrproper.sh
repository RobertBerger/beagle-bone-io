source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

echo "cleaning up - this takes some time"
echo "ccache extra flags: ${CCACHE_FLAGS}"
echo "+ make HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" mrproper"
make HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" mrproper
cd ${HERE}
