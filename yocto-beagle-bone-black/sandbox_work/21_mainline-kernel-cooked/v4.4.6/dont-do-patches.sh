echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/0001-allow-shared-ISR-for-int-16-gp_timer.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/0001-allow-shared-ISR-for-int-16-gp_timer.patch

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git status"
  git status
  echo "+ git diff"
  git diff
fi
