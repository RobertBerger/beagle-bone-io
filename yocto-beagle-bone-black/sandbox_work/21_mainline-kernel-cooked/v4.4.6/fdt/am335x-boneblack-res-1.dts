/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;

#include "am33xx.dtsi"
#include "am335x-bone-common.dtsi"

/* --> define input keys pinmux */

&am33xx_pinmux {
        gpio_keys_s0: gpio_keys_s0 {
                 pinctrl-single,pins = <
                        0x90 (PIN_INPUT_PULLUP | MUX_MODE7)  	/* .gpio2_2,  P8_7   66 $PIN: 36 INPUT_PULLUP MODE7 - nothing */
                 >;
        };
};

/* <-- define input keys pinmux */

/* --> define custom leds pinmux */
&am33xx_pinmux {
	traffic_leds_s0: traffic_leds_s0 {
                pinctrl-single,pins = <
			0x78 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio1_28, P9_12  60 $PIN:  30 OUTPUT MODE7 - traffic-1 LED */
                        0x44 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio1_17, P9_23  49 $PIN:  17 OUTPUT MODE7 - traffic-2 LED */
                       0x1A4 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio3_19, P9_27 115 $PIN: 105 OUTPUT MODE7 - traffic-3 LED */
                        0x34 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio1_13, P8_11  45 $PIN:  13 OUTPUT MODE7 - traffic-4 LED */
                        0x30 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio1_12, P8_12  44 $PIN:  12 OUTPUT MODE7 - traffic-5 LED */
                        0x28 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio0_26, P8_14  26 $PIN:  10 OUTPUT MODE7 - traffic-6 LED */
                        0x3c (PIN_OUTPUT_PULLDOWN | MUX_MODE7)  /* .gpio1_15, P8_15  47 $PIN   15 OUTPUT MODE7 - traffic-7 LED */
		>;
        };

};
/* <-- define custom leds pinmux */


&ldo3_reg {
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-always-on;
};

&mmc1 {
	vmmc-supply = <&vmmcsd_fixed>;
};

&mmc2 {
	vmmc-supply = <&vmmcsd_fixed>;
	pinctrl-names = "default";
	pinctrl-0 = <&emmc_pins>;
	bus-width = <8>;
	status = "okay";
	ti,vcc-aux-disable-is-sleep;
};

&am33xx_pinmux {
	nxp_hdmi_bonelt_pins: nxp_hdmi_bonelt_pins {
		pinctrl-single,pins = <
			0x1b0 0x03      /* xdma_event_intr0, OMAP_MUX_MODE3 | AM33XX_PIN_OUTPUT */
			0xa0 0x08       /* lcd_data0.lcd_data0, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xa4 0x08       /* lcd_data1.lcd_data1, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xa8 0x08       /* lcd_data2.lcd_data2, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xac 0x08       /* lcd_data3.lcd_data3, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb0 0x08       /* lcd_data4.lcd_data4, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb4 0x08       /* lcd_data5.lcd_data5, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb8 0x08       /* lcd_data6.lcd_data6, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xbc 0x08       /* lcd_data7.lcd_data7, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc0 0x08       /* lcd_data8.lcd_data8, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc4 0x08       /* lcd_data9.lcd_data9, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc8 0x08       /* lcd_data10.lcd_data10, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xcc 0x08       /* lcd_data11.lcd_data11, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd0 0x08       /* lcd_data12.lcd_data12, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd4 0x08       /* lcd_data13.lcd_data13, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd8 0x08       /* lcd_data14.lcd_data14, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xdc 0x08       /* lcd_data15.lcd_data15, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xe0 0x00       /* lcd_vsync.lcd_vsync, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xe4 0x00       /* lcd_hsync.lcd_hsync, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xe8 0x00       /* lcd_pclk.lcd_pclk, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xec 0x00       /* lcd_ac_bias_en.lcd_ac_bias_en, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
		>;
	};
	nxp_hdmi_bonelt_off_pins: nxp_hdmi_bonelt_off_pins {
		pinctrl-single,pins = <
			0x1b0 0x03      /* xdma_event_intr0, OMAP_MUX_MODE3 | AM33XX_PIN_OUTPUT */
		>;
	};
};

&lcdc {
	status = "okay";
};

/ {
	cpus {
		cpu@0 {
			cpu0-supply = <&dcdc2_reg>;
			/*
			 * To consider voltage drop between PMIC and SoC,
			 * tolerance value is reduced to 2% from 4% and
			 * voltage value is increased as a precaution.
			 */
			operating-points = <
				/* kHz    uV */
				1000000	1325000
				800000	1300000
				600000  1112000
				300000   969000
			>;
		};
	};

	hdmi {
		compatible = "ti,tilcdc,slave";
		i2c = <&i2c0>;
		pinctrl-names = "default", "off";
		pinctrl-0 = <&nxp_hdmi_bonelt_pins>;
		pinctrl-1 = <&nxp_hdmi_bonelt_off_pins>;
		status = "okay";

		panel-info {
			bpp = <16>;
			ac-bias = <255>;
			ac-bias-intrpt = <0>;
			dma-burst-sz = <16>;
			fdd = <16>;
			sync-edge = <1>;
			sync-ctrl = <1>;
			raster-order = <0>;
			fifo-th = <0>;
			invert-pxl-clk;
		};
	};

/* --> define input keys */

	gpio_keys {
        	pinctrl-names = "default";
        	pinctrl-0 = <&gpio_keys_s0>;

        	compatible = "gpio-keys";

                button@1 {
                        label = "button1";
                        debounce_interval = <50>;
                        gpios = <&gpio2 2 GPIO_ACTIVE_LOW>;
                        linux,code = <0x114>;
                        gpio-key,wakeup;
                };

	};

/* <-- define input keys */

/* --> define custom leds */

	gpio_leds {
		pinctrl-names = "default";
                pinctrl-0 = <&traffic_leds_s0>;

                compatible = "gpio-leds";

                led@1 {
                        label = "trfcl1:red";
                        gpios = <&gpio1 28 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@2 {
                        label = "trfcl1:amber";
                        gpios = <&gpio1 17 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@3 {
                        label = "trfcl1:green";
                        gpios = <&gpio3 19 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@4 {
                        label = "trfcl2:red";
                        gpios = <&gpio1 13 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@5 {
                        label = "trfcl2:amber";
                        gpios = <&gpio1 12 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@6 {
                        label = "trfcl2:green";
                        gpios = <&gpio0 26 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

                led@7 {
                        label = "trfcl3:red";
                        gpios = <&gpio1 15 GPIO_ACTIVE_HIGH>;
                        linux,default-trigger = "heartbeat";
                        default-state = "on";
                };

	};

/* <-- define custom leds */

};


