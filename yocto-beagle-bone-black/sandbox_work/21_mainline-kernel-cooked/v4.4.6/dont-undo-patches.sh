echo "+ git checkout arch/arm/mach-omap2/timer.c"
git checkout arch/arm/mach-omap2/timer.c

# patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch
# patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch
echo "+ git checkout arch/arm/boot/dts/am335x-boneblack.dts"
git checkout arch/arm/boot/dts/am335x-boneblack.dts

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git status"
  git status
  echo "+ git diff"
  git diff
fi
