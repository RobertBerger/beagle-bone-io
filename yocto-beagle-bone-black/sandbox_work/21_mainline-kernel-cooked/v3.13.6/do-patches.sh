# patches/arm/0001-deb-pkg-Simplify-architecture-matching-for-cross-bui.patch - debian packaging ???
#echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/arm/0001-deb-pkg-Simplify-architecture-matching-for-cross-bui.patch"
#git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/arm/0001-deb-pkg-Simplify-architecture-matching-for-cross-bui.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch

# patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch depends on 
# patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0001-reset-Add-driver-for-gpio-controlled-reset-pins.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0001-reset-Add-driver-for-gpio-controlled-reset-pins.patch

# depends on patches/sgx/0001-reset-Add-driver-for-gpio-controlled-reset-pins.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0002-prcm-port-from-ti-linux-3.12.y.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0002-prcm-port-from-ti-linux-3.12.y.patch

# depends on patches/sgx/0002-prcm-port-from-ti-linux-3.12.y.patch
# depends on patches/sgx/0001-reset-Add-driver-for-gpio-controlled-reset-pins.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0003-ARM-DTS-AM335x-Add-SGX-DT-node.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0003-ARM-DTS-AM335x-Add-SGX-DT-node.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0004-arm-Export-cache-flush-management-symbols-when-MULTI.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0004-arm-Export-cache-flush-management-symbols-when-MULTI.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0005-hack-port-da8xx-changes-from-ti-3.12-repo.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0005-hack-port-da8xx-changes-from-ti-3.12-repo.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0006-Revert-drm-remove-procfs-code-take-2.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0006-Revert-drm-remove-procfs-code-take-2.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0007-Changes-according-to-TI-for-SGX-support.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/sgx/0007-Changes-according-to-TI-for-SGX-support.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0001-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0001-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0002-saucy-disable-Werror-pointer-sign.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0002-saucy-disable-Werror-pointer-sign.patch

# depends on 
# patches/saucy/0002-saucy-disable-Werror-pointer-sign.patch
echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0003-saucy-disable-stack-protector.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/saucy/0003-saucy-disable-stack-protector.patch

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git diff"
  git diff
fi
