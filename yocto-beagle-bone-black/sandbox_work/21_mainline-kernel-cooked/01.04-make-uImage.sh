source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

echo "+ LOAD_ADDR = ${LOAD_ADDR}"
NUM_LOAD_ADDR=$(echo $((${LOAD_ADDR})))

if [ ${NUM_LOAD_ADDR} -eq 0 ]; then
   echo "building uImage without baked in LOAD_ADDR"
   echo "+ time make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" LOADADDR="${LOAD_ADDR}" CC="${CC}" uImage UIMAGE_TYPE=kernel_noload EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}"
   time make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" LOADADDR=${LOAD_ADDR} uImage UIMAGE_TYPE=kernel_noload EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}
else
   echo "building uImage with baked in LOAD_ADDR"
   echo "+ time make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" LOADADDR="${LOAD_ADDR}" CC="${CC}" uImage EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}"
   time make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" LOADADDR=${LOAD_ADDR} uImage EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}
fi

cd ${HERE}
