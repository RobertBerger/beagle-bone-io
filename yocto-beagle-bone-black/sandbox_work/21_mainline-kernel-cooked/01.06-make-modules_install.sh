source ../../env.sh

#EXTRAVER="-default"

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

echo "+ sudo make modules_install ARCH=${ARCH} INSTALL_MOD_PATH=/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo make modules_install ARCH=${ARCH} INSTALL_MOD_PATH=/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

cd ${HERE}

