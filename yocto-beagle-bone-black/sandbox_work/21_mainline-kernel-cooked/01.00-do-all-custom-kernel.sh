export DEVKIT="armv7a"

./01.01-make-mrproper.sh
./00.01-kernel-ver.sh
./01.02-patches-and-custom-config-kernel.sh
./01.03-make-menuconfig.sh
./01.04-make-uImage.sh
./01.05-make-modules.sh
./01.06-make-modules_install.sh
./01.07-make-fdt.sh
./02.00-cp-kernel-mainline-cooked.sh
