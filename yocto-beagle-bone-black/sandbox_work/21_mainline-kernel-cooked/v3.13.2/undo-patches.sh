# patches/dts/0001-arm-dts-am335x-boneblack-lcdc-add-panel-info.patch
# patches/dts/0002-arm-dts-am335x-boneblack-add-cpu0-opp-points.patch
echo "+ git checkout arch/arm/boot/dts/am335x-boneblack.dts"
git checkout arch/arm/boot/dts/am335x-boneblack.dts

# patches/dts/0003-arm-dts-am335x-bone-common-enable-and-use-i2c2.patch 
# depends on
# patches/dts/0004-arm-dts-am335x-bone-common-setup-default-pinmux-http.patch depends on
echo "+ git checkout arch/arm/boot/dts/am335x-bone-common.dtsi"
git checkout arch/arm/boot/dts/am335x-bone-common.dtsi

# patches/fixes/0001-pinctrl-pinctrl-single-must-be-initialized-early.patch
echo "+ git checkout drivers/pinctrl/pinctrl-single.c"
git checkout drivers/pinctrl/pinctrl-single.c

# patches/usb/0001-usb-musb-musb_host-Enable-ISOCH-IN-handling-for-AM33.patch
echo "+ git checkout drivers/usb/musb/musb_host.c"
git checkout drivers/usb/musb/musb_host.c

# patches/usb/0002-usb-musb-musb_cppi41-Make-CPPI-aware-of-high-bandwid.patch
# patches/usb/0003-usb-musb-musb_cppi41-Handle-ISOCH-differently-and-no.patch
echo "+ git checkout drivers/usb/musb/musb_cppi41.c"
git checkout drivers/usb/musb/musb_cppi41.c


# patches/sgx/0001-reset-Add-driver-for-gpio-controlled-reset-pins.patch
echo "+ git checkout drivers/reset/Kconfig"
git checkout drivers/reset/Kconfig
echo "+ git checkout drivers/reset/Makefile"
git checkout drivers/reset/Makefile
echo "+ rm -f Documentation/devicetree/bindings/reset/gpio-reset.txt"
rm -f  Documentation/devicetree/bindings/reset/gpio-reset.txt
echo "+ rm -f drivers/reset/gpio-reset.c"
rm -f drivers/reset/gpio-reset.c

# patches/sgx/0002-prcm-port-from-ti-linux-3.12.y.patch"
echo "+ git checkout arch/arm/boot/dts/am33xx.dtsi"
git checkout arch/arm/boot/dts/am33xx.dtsi
echo "+ git checkout drivers/reset/core.c"
git checkout drivers/reset/core.c
echo "+ git checkout include/linux/reset-controller.h"
git checkout include/linux/reset-controller.h
echo "+ git checkout include/linux/reset.h"
git checkout include/linux/reset.h
echo "+ rm -f Documentation/devicetree/bindings/arm/omap/prcm.txt"
rm -f Documentation/devicetree/bindings/arm/omap/prcm.txt
echo "+ rm -f drivers/reset/ti_reset.c"
rm -f drivers/reset/ti_reset.c

# patches/sgx/0004-arm-Export-cache-flush-management-symbols-when-MULTI.patch
echo "+ git checkout arch/arm/kernel/setup.c"
git checkout arch/arm/kernel/setup.c

# patches/sgx/0005-hack-port-da8xx-changes-from-ti-3.12-repo.patch
echo "+ git checkout drivers/video/da8xx-fb.c"
git checkout drivers/video/da8xx-fb.c
echo "+ git checkout include/video/da8xx-fb.h"
git checkout include/video/da8xx-fb.h

# patches/sgx/0006-Revert-drm-remove-procfs-code-take-2.patch
echo "+ git checkout drivers/gpu/drm/Makefile" 
git checkout drivers/gpu/drm/Makefile
echo "+ git checkout drivers/gpu/drm/drm_drv.c"
git checkout drivers/gpu/drm/drm_drv.c
echo "+ git checkout drivers/gpu/drm/drm_stub.c"
git checkout drivers/gpu/drm/drm_stub.c
echo "+ git checkout include/drm/drmP.h"
git checkout include/drm/drmP.h
echo "+ rm -f drivers/gpu/drm/drm_proc.c"
rm -f drivers/gpu/drm/drm_proc.c

# patches/sgx/0007-Changes-according-to-TI-for-SGX-support.patch
echo "+ git checkout arch/arm/mach-omap2/Makefile"
git checkout arch/arm/mach-omap2/Makefile
echo "+ git checkout arch/arm/mach-omap2/board-generic.c"
git checkout arch/arm/mach-omap2/board-generic.c
echo "+ git checkout arch/arm/mach-omap2/common.h"
git checkout arch/arm/mach-omap2/common.h
echo "+ rm -f arch/arm/mach-omap2/gpu.c"
rm -f arch/arm/mach-omap2/gpu.c

# patches/saucy/0001-saucy-error-variable-ilace-set-but-not-used-Werror-u.patch
echo "+ git checkout drivers/gpu/drm/omapdrm/omap_plane.c"
git checkout drivers/gpu/drm/omapdrm/omap_plane.c

# patches/saucy/0002-saucy-disable-Werror-pointer-sign.patch
echo "+ git checkout Makefile"
git checkout Makefile


if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git diff"
  git diff
fi
