#echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/lirc_bbb/0001-lirc_bbb.patch"
#git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/lirc_bbb/0001-lirc_bbb.patch

echo "+ git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/nunchuk/0001-Add-nunchuk-driver.patch"
git apply ${HERE}/${KERNEL_MAINLINE_VER}/patches/nunchuk/0001-Add-nunchuk-driver.patch

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git diff"
  git diff
  echo "+ git status"
  git status
  echo "+ press <ENTER> to go on"
  read r
fi
