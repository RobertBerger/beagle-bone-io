echo "+ cp ${HERE}/${KERNEL_MAINLINE_VER}/fdt/* arch/arm/boot/dts"
cp ${HERE}/${KERNEL_MAINLINE_VER}/fdt/* arch/arm/boot/dts

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
fi
#else
#  echo "+ git diff"
#  git diff
#fi
