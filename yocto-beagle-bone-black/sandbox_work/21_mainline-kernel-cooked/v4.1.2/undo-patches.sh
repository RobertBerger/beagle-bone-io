# patches/lirc_bbb/0001-lirc_bbb.patch

#echo "+ git checkout drivers/staging/media/lirc/Kconfig"
#git checkout drivers/staging/media/lirc/Kconfig

#echo "+ git checkout drivers/staging/media/lirc/Makefile"
#git checkout drivers/staging/media/lirc/Makefile

#echo "+ rm -f drivers/staging/media/lirc/lirc_bbb.c"
#rm -f drivers/staging/media/lirc/lirc_bbb.c

# patches/nunchuk/0001-Add-nunchuk-driver.patch:

echo "+ git checkout drivers/input/joystick/Kconfig"
git checkout drivers/input/joystick/Kconfig

echo "+ git checkout drivers/input/joystick/Makefile"
git checkout drivers/input/joystick/Makefile

echo "+ rm -f drivers/input/joystick/wiichuck.c"
rm -f drivers/input/joystick/wiichuck.c

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make diff, but now I skip it"
else
  echo "+ git status"
  git status
  echo "+ git diff"
  git diff
fi
