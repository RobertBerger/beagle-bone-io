/*
 * Copyright (C) 2012 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */
/dts-v1/;

#include "am33xx.dtsi"
#include "am335x-bone-common.dtsi"

/* --> define input keys pinmux */

&am33xx_pinmux {
        gpio_keys_s0: gpio_keys_s0 {
                 pinctrl-single,pins = <
                        0x90 (PIN_INPUT_PULLUP | MUX_MODE7)  	/* .gpio2_2,  P8_7   66 $PIN: 36 INPUT_PULLUP MODE7 - Switch */
                 >;
        };
};

/* <-- define input keys pinmux */

/* --> define rotary_encoder pins */
&am33xx_pinmux {
         rotary_encoder_pins: pinmux_rotary_encoder_pins {
                  pinctrl-single,pins = <
                       0x38 (PIN_INPUT_PULLUP | MUX_MODE7)     /* .gpio1_14,  P8_16 Linux GPIO NO: 46 $PIN: 14 INPUT MODE7 - rotary encoder A */
                       0x3c (PIN_INPUT_PULLUP | MUX_MODE7)     /* .gpio1_15,  P8_15 Linux GPIO NO: 47 $PIN: 15 INPUT MODE7 - rotary encoder B */
                  >;
         };
};       
/* <-- define rotary_encoder pins */



/* --> define hd44780 pinmux */
/*
&am33xx_pinmux {
	hd44780_pins: pinmux_hd4470_pins {
                pinctrl-single,pins = <
                        0x78 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_28, P9_12  60 GPIO NO:  30 OUTPUT MODE7 - hd44780_rs    
                        0x44 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_17, P9_23  49 GPIO NO:  17 OUTPUT MODE7 - traffic-2 LED 
                       0x1A4 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio3_19, P9_27 115 GPIO NO: 105 OUTPUT MODE7 - traffic-3 LED 
                        0x34 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_13, P8_11  45 GPIO NO:  13 OUTPUT MODE7 - traffic-4 LED 
                        0x30 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_12, P8_12  44 GPIO NO:  12 OUTPUT MODE7 - traffic-5 LED 
                        0x28 (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio0_26, P8_14  26 GPIO NO:  10 OUTPUT MODE7 - traffic-6 LED 
                        0x3c (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_15, P8_15  47 GPIO NO:  15 OUTPUT MODE7 - traffic-7 LED 
		>;
        };
};
*/
/* <-- define hd44780 pinmux */

/* --> define pwm2 pins pinmux */

&am33xx_pinmux {
         ehrpwm2_pin_p8_13: pinmux_ehrpwm2_pin_p8_13 {
                pinctrl-single,pins = <
                       0x024 0x4                                /* P8_13: MODE 4: EHRPWM2B */
               >;
         };

         ehrpwm2_pin_p8_19: pinmux_ehrpwm2_pin_p8_19 {
                pinctrl-single,pins = <
                       0x020 0x4                                /* P8_19: MODE 4: EHRPWM2A */
               >;
         };

};

/* <-- define pwm2 pins pinmux */


&ldo3_reg {
	regulator-min-microvolt = <1800000>;
	regulator-max-microvolt = <1800000>;
	regulator-always-on;
};

&mmc1 {
	vmmc-supply = <&vmmcsd_fixed>;
};

&mmc2 {
	vmmc-supply = <&vmmcsd_fixed>;
	pinctrl-names = "default";
	pinctrl-0 = <&emmc_pins>;
	bus-width = <8>;
	status = "okay";
	ti,vcc-aux-disable-is-sleep;
};

&am33xx_pinmux {
	nxp_hdmi_bonelt_pins: nxp_hdmi_bonelt_pins {
		pinctrl-single,pins = <
			0x1b0 0x03      /* xdma_event_intr0, OMAP_MUX_MODE3 | AM33XX_PIN_OUTPUT */
			0xa0 0x08       /* lcd_data0.lcd_data0, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xa4 0x08       /* lcd_data1.lcd_data1, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xa8 0x08       /* lcd_data2.lcd_data2, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xac 0x08       /* lcd_data3.lcd_data3, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb0 0x08       /* lcd_data4.lcd_data4, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb4 0x08       /* lcd_data5.lcd_data5, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xb8 0x08       /* lcd_data6.lcd_data6, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xbc 0x08       /* lcd_data7.lcd_data7, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc0 0x08       /* lcd_data8.lcd_data8, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc4 0x08       /* lcd_data9.lcd_data9, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xc8 0x08       /* lcd_data10.lcd_data10, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xcc 0x08       /* lcd_data11.lcd_data11, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd0 0x08       /* lcd_data12.lcd_data12, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd4 0x08       /* lcd_data13.lcd_data13, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xd8 0x08       /* lcd_data14.lcd_data14, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xdc 0x08       /* lcd_data15.lcd_data15, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT | AM33XX_PULL_DISA */
			0xe0 0x00       /* lcd_vsync.lcd_vsync, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xe4 0x00       /* lcd_hsync.lcd_hsync, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xe8 0x00       /* lcd_pclk.lcd_pclk, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
			0xec 0x00       /* lcd_ac_bias_en.lcd_ac_bias_en, OMAP_MUX_MODE0 | AM33XX_PIN_OUTPUT */
		>;
	};
	nxp_hdmi_bonelt_off_pins: nxp_hdmi_bonelt_off_pins {
		pinctrl-single,pins = <
			0x1b0 0x03      /* xdma_event_intr0, OMAP_MUX_MODE3 | AM33XX_PIN_OUTPUT */
		>;
	};
};

&lcdc {
	status = "okay";
};

/ {
	cpus {
		cpu@0 {
			cpu0-supply = <&dcdc2_reg>;
			/*
			 * To consider voltage drop between PMIC and SoC,
			 * tolerance value is reduced to 2% from 4% and
			 * voltage value is increased as a precaution.
			 */
			operating-points = <
				/* kHz    uV */
				1000000	1325000
				800000	1300000
				600000  1112000
				300000   969000
			>;
		};
	};

	hdmi {
		compatible = "ti,tilcdc,slave";
		i2c = <&i2c0>;
		pinctrl-names = "default", "off";
		pinctrl-0 = <&nxp_hdmi_bonelt_pins>;
		pinctrl-1 = <&nxp_hdmi_bonelt_off_pins>;
		status = "okay";

		panel-info {
			bpp = <16>;
			ac-bias = <255>;
			ac-bias-intrpt = <0>;
			dma-burst-sz = <16>;
			fdd = <16>;
			sync-edge = <1>;
			sync-ctrl = <1>;
			raster-order = <0>;
			fifo-th = <0>;
			invert-pxl-clk;
		};
	};

/* --> define input keys */

	gpio_keys {
        	pinctrl-names = "default";
        	pinctrl-0 = <&gpio_keys_s0>;

        	compatible = "gpio-keys";

                button@1 {
                        label = "button1";
                        debounce_interval = <50>;
                        gpios = <&gpio2 2 GPIO_ACTIVE_LOW>;
                        linux,code = <0x114>;
                        gpio-key,wakeup;
                };

	};

/* <-- define input keys */

/* --> define hd44780 gpios */
/*
       hd44780-gpios {
               pinctrl-names = "default";
               pinctrl-0 = <&hd44780_pins>;

               compatible = "gpio";
               led@1 {
                        label = "hd44780_rs";
                        gpios = <&gpio1 28 GPIO_ACTIVE_LOW>;
                        linux,default-trigger = "none";
                        default-state = "off";
               };

       };
*/
/* <-- define hd44780 gpios */

/* --> define custom leds */
/*
	gpio_leds {
		pinctrl-names = "default";
                pinctrl-0 = <&hd44780_pins>;

                compatible = "gpio-leds";

                led@1 {
                        label = "trfcl1:red";
                        gpios = <&gpio1 28 GPIO_ACTIVE_LOW>;
                        linux,default-trigger = "none";
                        default-state = "off";
                };

	};
*/

/*
                        0x3c (PIN_OUTPUT_PULLDOWN | MUX_MODE7)     .gpio1_15, P8_15  47 GPIO NO:  15 OUTPUT MODE7 - traffic-7 LED
*/


/* <-- define custom leds */

       hd44780_gpio: hd44780_gpio@0 {
                compatible = "gpio-hd44780";

                rs = < &gpio1 28 GPIO_ACTIVE_HIGH         /* .gpio1_28, P9_12  Linux GPIO#: 60 $PINS:  30 */ >;
                 e = < &gpio1 17 GPIO_ACTIVE_HIGH         /* .gpio1_17, P9_23  Linux GPIO#: 49 $PINS:  17 */ >;
               db4 = < &gpio3 19 GPIO_ACTIVE_HIGH         /* .gpio3_19, P9_27  Linux GPIO#:115 $PINS: 105 */ >;
               db5 = < &gpio1 13 GPIO_ACTIVE_HIGH         /* .gpio1_13, P8_11  Linux GPIO#: 45 $PINS:  13 */ >;
               db6 = < &gpio1 12 GPIO_ACTIVE_HIGH         /* .gpio1_12, P8_12  Linux GPIO#: 44 $PINS:  12 */ >;
               db7 = < &gpio0 26 GPIO_ACTIVE_HIGH         /* .gpio0_26, P8_14  Linux GPIO#: 26 $PINS:  10 */ >;
        };

        rotary_encoder {
                compatible = "rotary-encoder";
                pinctrl-names = "default";
                pinctrl-0 = <&rotary_encoder_pins>;
                gpios = <&gpio1 14 /*GIO_ACTIVE_HIGH*/ GPIO_ACTIVE_LOW>, <&gpio1 15 GPIO_ACTIVE_LOW>;
                rotary-encoder,steps = <25>;
                linux,axis = <1>; /* REL_Y */
                rotary-encoder,relative-axis;
                /* rotary-encoder,rollover; */
                /* rotary-encoder,half-period; */
        };
};

/* --> pwm2 */
&epwmss2 {
        pinctrl-names = "default";
        pinctrl-0 = <
                &ehrpwm2_pin_p8_19
                &ehrpwm2_pin_p8_13
        >;

        status = "okay";

        ehrpwm@48304200 {
                status = "okay";
        };
};
/* <-- pwm2 */


