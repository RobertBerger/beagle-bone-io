sudo apt-get install ncurses-dev libncursesw5-dev

source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally make menuconfig, but now I make make oldconfig"
  echo "+ make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" oldconfig"
  make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" oldconfig 
else
  echo "+ make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" menuconfig"
  make -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" menuconfig
fi

cd ${HERE}
