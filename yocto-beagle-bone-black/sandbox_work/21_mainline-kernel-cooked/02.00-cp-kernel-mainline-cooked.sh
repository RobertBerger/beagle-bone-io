source ../../env.sh

HERE=(`pwd`)

echo "Linux version:"
strings ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/Image | grep "Linux version"

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally wait here, but not with jenkins"
else
  echo "do you really want to copy uImage with the version above to /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/ ?"
  echo "press <ENTER> to go on"
  read r
fi

if [ -f ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage ];
then
  echo "(custom config) ${CUSTOM_KERNEL_NAME}-working:"
  strings ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/Image | grep "Linux version"
  sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/uImage
  sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/uImage-${CUSTOM_KERNEL_NAME}-working
  # in order to find with strings the kernel version:
  sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/Image /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/Image-${CUSTOM_KERNEL_NAME}-working
  # cp also over to tftp
  cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage ${TFTP_PREFIX}/${TARGET_BOARD}/uImage-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID}
  cd ${TFTP_PREFIX}/${TARGET_BOARD}
  ln -sf uImage-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID} uImage
  echo "+ ls -la ${TFTP_PREFIX}/${TARGET_BOARD}"
  ls -la ${TFTP_PREFIX}/${TARGET_BOARD}
else
  echo "-------------> no ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/uImage? <--------------------"
fi 

if [ -f ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT_NAME} ];
then
  echo "(custom config) fdt"
  echo "+ sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT_NAME} /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/${FDT_NAME}"
  sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT_NAME} /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/${FDT_NAME}
  sudo cp ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT_NAME} /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/${FDT_NAME}-${CUSTOM_KERNEL_NAME}-working.dtb
  # cp also over to tftp
  cp  ${CUSTOM_KERNELDIR}/arch/${ARCH}/boot/dts/${FDT_NAME} ${TFTP_PREFIX}/${TARGET_BOARD}/${FDT_NAME}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID}
  cd ${TFTP_PREFIX}/${TARGET_BOARD}
  ln -sf ${FDT_NAME}-${CUSTOM_KERNEL_NAME}-${KERNEL_MAINLINE_VER}-${TRAINING_ID} uImage-am335x-boneblack.dtb
  echo "+ ls -la ${TFTP_PREFIX}/${TARGET_BOARD}"
  ls -la ${TFTP_PREFIX}/${TARGET_BOARD}
else
  echo "no fdt"
fi

ls -la /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}/boot/

cd ${HERE}

# update also EXTRA_STUFF
# ../03-SD-card/01.06-update-rootfs-from-opt.sh

