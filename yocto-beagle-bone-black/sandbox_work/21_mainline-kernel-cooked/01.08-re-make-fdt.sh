source ../../env.sh

HERE=$(pwd)

source ${HERE}/01-build-kernel-common.sh

cd ${CUSTOM_KERNELDIR}

echo "+ ls ${HERE}"
ls ${HERE}

if [ -d ${HERE}/${KERNEL_MAINLINE_VER}/fdt ]; then
  echo "+ found some extra fdt stuff for this kernel"
  source ${HERE}/${KERNEL_MAINLINE_VER}/do-fdt.sh
else
  echo "+ no extra fdt stuff found for this kernel"
fi

#for FDT in ${FDT_NAME} am335x-boneblack-res-1.dtb am335x-boneblack-res-2.dtb am335x-boneblack-res-3.dtb am335x-boneblack-res-4.dtb
for FDT in ${FDT_NAME} am335x-boneblack-res-1.dtb am335x-boneblack-res-2.dtb am335x-boneblack-res-3.dtb
do
  echo " ---> ${FDT}"
  echo "+ rm -f arch/arm/boot/dts/${FDT}"
  rm -f arch/arm/boot/dts/${FDT} 

  echo "+ make V=1 -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" ${FDT} EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}"
  time make V=1 -j $(getconf _NPROCESSORS_ONLN) HOSTCC="${HOSTCC}" HOSTCXX="${HOSTCXX}" CC="${CC}" ${FDT} EXTRAVERSION=-${CUSTOM_EXTRAVER}-${USER}
  echo " <-- ${FDT}"
done

echo "+ vim ./arch/arm/boot/dts/.am335x-boneblack-res-3.dtb.dts.tmp ?"

read -r -p "Are you sure? [y/N] " response
case $response in
    [yY][eE][sS]|[yY]) 
        vim ./arch/arm/boot/dts/.am335x-boneblack-res-3.dtb.dts.tmp
        ;;
    *)
        echo "continue"
        ;;
esac

cd ${HERE}
