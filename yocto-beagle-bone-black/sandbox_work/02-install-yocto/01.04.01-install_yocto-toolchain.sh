HERE=`pwd`

source ../../env.sh

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally wait for input from command line"
else
  echo "going to erase /opt/${TRAINING_ID}/${YOCTO_VERSION}" 
  echo "press <ENTER> to go on"
  read r
fi
sudo rm -rf /opt/${TRAINING_ID}/${YOCTO_VERSION}

echo "+ cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}"
cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}

echo "+ cd toolchain/x86_64"
cd toolchain/x86_64
echo "+ ./${YOCTO_TOOLCHAIN} -d /opt/${TRAINING_ID}/${YOCTO_VERSION} -y"
./${YOCTO_TOOLCHAIN} -d /opt/${TRAINING_ID}/${YOCTO_VERSION} -y

cd ${HERE}
