HERE=`pwd`

source ../../env.sh

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally wait for input from command line"
else
  echo "going to erase /opt/${YOCTO_VERSION}" 
  echo "press <ENTER> to go on"
  read r
fi
sudo rm -rf /opt/${YOCTO_VERSION}

echo "+ cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}"
cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}

echo "+ cd toolchain/x86_64"
cd toolchain/x86_64
echo "+ ./${YOCTO_TOOLCHAIN} -y"
./${YOCTO_TOOLCHAIN} -y

echo "+ cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}"
cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}

echo "+ cd machines/${YOCTO_ML}"
cd machines/${YOCTO_ML}

echo "+ sudo mkdir -p /opt/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo mkdir -p /opt/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

echo "+ sudo tar xjf ${TARGET_CONFIGURATION}-${YOCTO_ML}.tar.bz2 -C /opt/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo tar xjf ${TARGET_CONFIGURATION}-${YOCTO_ML}.tar.bz2 -C /opt/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

echo "+ sudo mkdir /opt/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
sudo mkdir /opt/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}

echo "+ sudo tar xjf core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}.tar.bz2 -C /opt/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
sudo tar xjf core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}.tar.bz2 -C /opt/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}

cd ${HERE}
