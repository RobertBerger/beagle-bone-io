(*-->source ~/<TRAINING_ID>/<TOOLKIT>-<TARGET_BOARD>/<TRAINING_ID_SHORT>_for_student/env.sh<--*) (*->\label{env-script}<-*)
sudo rm -rf /opt/<YOCTO_VERSION>

# install toolchain and rootfs
cd ${HOME}/yocto-download/<YOCTO_BUILD_VERSION>

# install toolchain
cd toolchain/x86_64
./<YOCTO_TOOLCHAIN>

cd ${HOME}/yocto-download/<YOCTO_BUILD_VERSION>
cd machines/<YOCTO_ML>

# install <TARGET_CONFIGURATION> rootfs (nfs)
sudo mkdir /opt/<YOCTO_VERSION>/<TARGET_CONFIGURATION>-<YOCTO_ML>
sudo tar xjvf <TARGET_CONFIGURATION>-<YOCTO_ML>.tar.bz2 -C /opt/<YOCTO_VERSION>/<TARGET_CONFIGURATION>-<YOCTO_ML>

# install core-image-<TARGET_CONFIGURATION_SHORT_MINIMAL_DEV> rootfs (rootfs chapter)
sudo mkdir /opt/<YOCTO_VERSION>/core-image-<TARGET_CONFIGURATION_SHORT_MINIMAL_DEV>-<YOCTO_ML>
sudo tar xjf core-image-<TARGET_CONFIGURATION_SHORT_MINIMAL_DEV>-<YOCTO_ML>.tar.bz2 -C /opt/<YOCTO_VERSION>/core-image-<TARGET_CONFIGURATION_SHORT_MINIMAL_DEV>-<YOCTO_ML>

