source env.sh
----
TARGET_ARCHITECTURE=<TARGET_ARCHITECTURE>
ELDK_FTP_PATH=<ELDK_FTP_PATH>
ELDK_TOOLCHAIN=<ELDK_TOOLCHAIN>
ELDK_IMAGE=<ELDK_IMAGE>
----
mkdir ${HOME}/eldk-download
cd ${HOME}/eldk-download
mkdir -p targets/${TARGET_ARCHITECTURE}
wget ${ELDK_FTP_PATH}/install.sh
cd targets/${TARGET_ARCHITECTURE}
wget ${ELDK_FTP_PATH}/targets/${TARGET_ARCHITECTURE}/target.conf
wget ${ELDK_FTP_PATH}/targets/${TARGET_ARCHITECTURE}/${ELDK_TOOLCHAIN}
wget ${ELDK_FTP_PATH}/targets/${TARGET_ARCHITECTURE}/${ELDK_IMAGE}
