HERE=`pwd`

source ../../env.sh

if [[ $WORKSPACE = *jenkins* ]]; then
  echo "WORKSPACE '$WORKSPACE' contains jenkins"
  echo "would normally wait for input from command line"
else
  echo "going to erase /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}" 
# echo "and /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
  echo "going to install ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}/machines/${YOCTO_ML_MULTIARCH}/${TARGET_CONFIGURATION}-${YOCTO_ML_MULTIARCH}.tar.bz2"
# echo "and ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}/machines/${YOCTO_ML_MULTIARCH}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML_MULTIARCH}.tar.bz2"
  echo "press <ENTER> to go on"
  read r
fi

echo "+ sudo rm -rf /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo rm -rf /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}
#echo "+ sudo rm -rf /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
#sudo rm -rf /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}

echo "+ cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}"
cd ${HOME}/yocto-download/${YOCTO_BUILD_VERSION}

echo "+ cd machines/${YOCTO_ML_MULTIARCH}"
cd machines/${YOCTO_ML_MULTIARCH}

echo "+ sudo mkdir -p /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo mkdir -p /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

echo "+ sudo tar xjf ${TARGET_CONFIGURATION}-${YOCTO_ML_MULTIARCH}.tar.bz2 -C /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
sudo tar xjf ${TARGET_CONFIGURATION}-${YOCTO_ML_MULTIARCH}.tar.bz2 -C /opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}

#echo "+ sudo mkdir /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
#sudo mkdir /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}

#echo "+ sudo tar xjf core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML_MULTIARCH}.tar.bz2 -C /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}"
#sudo tar xjf core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML_MULTIARCH}.tar.bz2 -C /opt/${TRAINING_ID}/${YOCTO_VERSION}/core-image-${TARGET_CONFIGURATION_SHORT_MINIMAL_DEV}-${YOCTO_ML}

cd ${HERE}
