# TODO: restore PATH after calling ELDK script
echo "you need to source it: source env.sh"

export TRAINING_ID="beagle-bone-io-2.0"
export INTERNET="no"

file="/home/$USER/PATH.ori"
if [ -e $file ]; then
	echo "File exists"
        PATH=`cat $file`
else 
	echo "File does not exists"
        echo $PATH > $file
fi 

export MAKE_DIR="../../make"

export SD_RW="sdb"

# those variables can come from the environment not to wait with select

if [ ! -n "$DEVKIT" ]; then
  echo "DEVKIT is blank"
  echo "Select Development kit:"
  select DEVKIT in 'armv7a' 'native'
  do
    echo "DEVKIT is $DEVKIT"
    #export DEVKIT=$DEVKIT
    break;
  done
else
  echo "--> DEVKIT is $DEVKIT <--"
fi

#export EXTRA_STUFF="${HOME}/projects/elin-extra-stuff"

#echo "Select Development kit:"
#select DEVKIT in 'armv7a' 'native'
#	do
	  if [ "$DEVKIT" == "armv7a" ]; then
            echo "not native, but armv7a"

            export YOCTO_ML="beagle-bone-black-ml"
            export YOCTO_ML_MULTIARCH="multi-v7-ml"
            export YOCTO_BUILD_VERSION="20160318-1"
            export YOCTO_VERSION_NUMBER="2.0.1"
            export YOCTO_VERSION="poky/${YOCTO_VERSION_NUMBER}"
            export TARGET_ARCHITECTURE="armv7a"
            export TARGET_CONF="core-image-sato"
            export TARGET_CONFIGURATION="core-image-sato-sdk"
            export TARGET_CONFIGURATION_SHORT="sato-sdk"
            export TARGET_CONFIGURATION_SHORT_MINIMAL_DEV="minimal-dev"
            export YOCTO_TOOLCHAIN="poky-glibc-x86_64-${TARGET_CONF}-${TARGET_ARCHITECTURE}-vfp-neon-toolchain-${YOCTO_VERSION_NUMBER}.sh"
            export TARGET_BOARD="beagle-bone-black"

#           echo "+ source /opt/${YOCTO_VERSION}/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi"
            echo "+ source /opt/${TRAINING_ID}/${YOCTO_VERSION}/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi"


            if [ -e /opt/${TRAINING_ID}/${YOCTO_VERSION}/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi ]; then
              source /opt/${TRAINING_ID}/${YOCTO_VERSION}/environment-setup-armv7a-vfp-neon-poky-linux-gnueabi
            fi


            echo
            echo "CC:"
            export | grep CC
            echo
            echo "PATH:"
            export | grep PATH
            echo
            echo "ARCH:"
            export ARCH=arm
            export | grep ARCH
            echo
            echo "CROSS_COMPILE:"
            export CROSS_COMPILE=arm-poky-linux-gnueabi-
            export | grep CROSS_COMPILE

            #export UBOOT_MAINLINE_VER="v2011.06"
	    #export UBOOT_MAINLINE_VER="v2011.12"
            #export UBOOT_MAINLINE_VER="v2012.04.01"
            #export UBOOT_MAINLINE_VER="v2012.10"
            export UBOOT_MAINLINE_VER="v2016.01"
            export UBOOT_CONFIG="am335x_evm_config"

	    #export KERNEL_MAINLINE_VER="v3.13.6"
            #export KERNEL_MODULES_DIR="3.13.6-${USER}"

            export KERNEL_MAINLINE_VER="v4.4.6"
#            export KERNEL_MODULES_DIR="3.14.22-${USER}"
#            export KERNEL_MAINLINE_VER_ON_TARGET="3.14.22"

#            export KERNEL_MAINLINE_VER="v3.17-rc6"
#            export KERNEL_MODULES_DIR="3.17-rc6-${USER}"

            export LINUX_CONSOLE="ttyO0"

            export FDT_NAME="am335x-boneblack.dtb"

            export KERNEL_DEFCONFIG_SHORT="omap"
            export KERNEL_DEFCONFIG="omap2plus_defconfig"
            #export FDT_NAME="dtbs"

           # mkimage -e UBOOT_ENTRYPOINT
            export UBOOT_ENTRYPOINT="0x80008000"
            # mkimage -a <UBOOT_LOADADDRESS>
            export UBOOT_LOADADDRESS="0x80008000"
            # what's passed to the kernel for mkimage -a i.e. UBOOT_ENTRYPOINT:
            export LOAD_ADDR="0x80008000"
            # whereto in u-boot we'll load the kernel to e.g. from tftp:
            export KERNEL_ADDR_R="0x80300000"
            # whereto in u-boot we'll load the ramdisk e.g. from tftp:
            export ROOTFS_ADDR_R="0x85000000"
            # whereto in u-boot we'll load the fdt
            export FDT_ADDR_R="0x815f0000"

	    export ANGSTROM_ROOTFS="Angstrom-Beagleboard-demo-image-glibc-ipk-2011.1-beagleboard.rootfs.tar.bz2"

            #export EXTRA_STUFF="${HOME}/projects/elin-extra-stuff"
            #@@@ link kernel modules against the right kernel !!!!
           export KERNEL_GIT_REPO="git://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git"
           export GITPOD_KERNEL_GIT_REPO="gp@gitpod:linux-stable.git"

#            export KERNEL_GIT_REPO="git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git"
#            export GITPOD_KERNEL_GIT_REPO="gp@gitpod:linux.git"

            export KERNEL_NAME="linux-stable"
            export KERNELDIR="${HOME}/${TARGET_BOARD}/${KERNEL_NAME}"
            export KERNEL_EXTRAVER="stable"
   
            export CUSTOM_KERNEL_NAME="linux-stable-custom"
            export CUSTOM_KERNELDIR="${HOME}/${TARGET_BOARD}/${CUSTOM_KERNEL_NAME}"
            export CUSTOM_EXTRAVER="custom"

            export PREEMPT_RT_GIT_REPO="git://git.kernel.org/pub/scm/linux/kernel/git/rt/linux-stable-rt.git"
            export PREEMPT_RT_KERNEL_NAME="linux-stable-rt"
            export PREEMPT_RT_KERNELDIR="${HOME}/${TARGET_BOARD}/${PREEMPT_RT_KERNEL_NAME}"
            export PREEMPT_RT_EXTRAVER="prt"

            export NFS_EXPORTED_ROOFS_ROOT="/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"
            export NFS_EXPORTED_ROOTFS_ROOT="/opt/${TRAINING_ID}/${YOCTO_VERSION}/${TARGET_CONFIGURATION}-${YOCTO_ML}"

            export EXTRA_STUFF="${HOME}/${TRAINING_ID}-extra-stuff/${TARGET_BOARD}"
            export EXTRA_STUFF_SHORT="${TRAINING_ID}-extra-stuff/${TARGET_BOARD}"

            # that's currently armv7a-elin
            # --> vlab-1
            export BUILDHOST="`ifconfig  | grep 'inet addr:'| grep -v '127.0.0.1' | cut -d: -f2 | awk '{ print $1}'`"
            #echo "BUILDHOST: ${BUILDHOST}"
            export SUBNET=`echo  ${BUILDHOST} | cut -d"." -f1-3`
            #echo "SUBNET: ${SUBNET}"

            export BUILDHOST_HARDCODED="${SUBNET}.23"

            #export SUBNET="192.168.44"
            #export BUILDHOST="${SUBNET}.25"
            export BUILDHOST_USER="student"

            if [[ $INTERNET != *no* ]]; then
                echo "+ I think I have INTERNET access"
                export TARGET_IP_MAC_LAST="11"
	        else
		        echo "+ no INTERNET access"
		        export TARGET_IP_MAC_LAST="11"
            fi
            # <-- vlab-1

            export SERVER_IP=${BUILDHOST}
            export SERVER_IP_HARDCODED="192.168.42.1"

            export GATEWAY_IP="${SERVER_IP}"
            export GATEWAY_IP_HARDCODED="${SERVER_IP_HARDCODED}"

            export TARGET_MAC="0:0:1:2:3:${TARGET_IP_MAC_LAST}"
            export TARGET_MAC_HARDCODED="0:0:1:2:3:11"

            export TARGET_IP="${SUBNET}.${TARGET_IP_MAC_LAST}"
            export TARGET_IP_HARDCODED="192.168.42.11"

            export TFTP_PREFIX="/tftpboot"
            export STRESS_VERSION="1.0.4"
     
	  fi # DEVKIT != native
          if [ "$DEVKIT" == "native" ]; then
            echo "native"
            unset ARCH
	    unset CROSS_COMPILE
            unset DEPMOD
            unset KERNELDIR
            unset CC
            unset CFLAGS
            unset CONFIG_SITE
            export ARCH=i386
            export TARGET_BOARD="x61s"
            export EXTRA_STUFF="${HOME}/${TRAINING_ID}-extra-stuff/${TARGET_BOARD}"
          fi # DEVKIT == native
#	break;
#done

