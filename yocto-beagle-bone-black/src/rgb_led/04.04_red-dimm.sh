set -x
HERE=$(pwd)
cd /sys/class/pwm/pwmchip0/pwm0
echo 0 > enable
echo 10000000 > period
echo 1000000 > duty_cycle
echo 1 > enable
cd ${HERE}
set +x

