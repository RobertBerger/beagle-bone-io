set -x
HERE=$(pwd)
cd /sys/class/pwm/pwmchip2/pwm1
echo 0 > enable
echo 10000000 > period
echo 960000 > duty_cycle
echo 1 > enable
cd ${HERE}
set +x

