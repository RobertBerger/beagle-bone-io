set -x
HERE=$(pwd)
cd /sys/class/pwm/pwmchip2/pwm1
set +x 
echo "+ echo 0 > enable"
echo 0 > enable
echo "+ echo 10000000 > period"
echo 10000000 > period
echo "+ echo 10000000 > duty_cycle"
echo 10000000 > duty_cycle
echo "echo 1 > enable"
echo 1 > enable
cd ${HERE}
set +x

