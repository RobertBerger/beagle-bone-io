set -x
HERE=$(pwd)
cd /sys/class/pwm/pwmchip2/pwm0
echo 0 > enable
echo 10000000 > period
echo 10000000 > duty_cycle
echo 1 > enable
cd ${HERE}
set +x

