/*
 * HD44780 LCD/GPIO driver for Linux
 *  Robert Berger <robert.berger@reliableembeddedsystems.com>
 *
 *  adapted from hd44780 driver using gpio pins -  bifferos@yahoo.co.uk, 2008
 *
 */

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include "gpio_from_kernel_fdt.h"
#include <linux/delay.h>

#define MYDEV_NAME "gpio_test_fdt_dev"

/* note that those pins need to initialized to be GPIOs in the fdt */

/*  Pin ID     Linux GPIO number      Beagle Bone Black pin annotation */
#define HD_RS      49 /*30*/                 /* traffic_leds_s0    p9_11 gpio Bank0.Pin30 PIN#30 */        
/* #define HD_RW      14                 not needed - always write active */
/*
#define HD_E       15              
#define HD_DB4     17              
#define HD_DB5     18              
#define HD_DB6     20              
#define HD_DB7     21              
*/


/* Structures to hold the pins we've successfully allocated. */
typedef struct PinSet {
  int pin;      /* Linux GPIO pin number */
  char* name;   /* Name of the pin, supplied to gpio_request() */
  int result;   /* set to zero on successfully obtaining pin */
} tPinSet;


static tPinSet pins[] = {
  {HD_RS,  "HD44780_RS",  -1},
};

#define PINS_NEEDED (sizeof(pins)/sizeof(tPinSet))

// Macros for setting control lines
#define HD_RS_LOW  gpio_set_value(HD_RS, 0);
#define HD_RS_HIGH gpio_set_value(HD_RS, 1);
#define HD_E_LOW   gpio_set_value(HD_E, 0);
#define HD_E_HIGH  gpio_set_value(HD_E, 1);

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE
};

static struct miscdevice my_misc_device = {
        .minor = MISC_DYNAMIC_MINOR,
        .name = MYDEV_NAME,
        .fops = &mycdrv_fops,
};

/* return any acquired pins. */
static void FreePins(void)
{
  int i;
  for (i=0;i<PINS_NEEDED;i++)
  {
    if (pins[i].result == 0)
    {
      gpio_free(pins[i].pin);
      pins[i].result = -1;    /* defensive programming - avoid multiple free. */
    }
  }
}
#if 1
static int __init gpio_test_init(void)
{
	int i;
	int got_pins = 1;
        struct device_node *np =dev->of_node;

        display_np = of_parse_phandle(np, "hd44780", 0);
         if (!display_np) {
                 pr_err("failed to find hd44780 phandle\n");
                 return -ENOENT;
        } 

        if (misc_register(&my_misc_device)) {
                pr_warn("Couldn't register device misc, %d.\n",
                my_misc_device.minor);
                return -EBUSY;
        }

        pr_info("PINS_NEEDED %d\n",PINS_NEEDED);
	/* Request pins */
  	for (i=0;i<PINS_NEEDED;i++) {
                if (gpio_is_valid(pins[i].pin)) {
    		  pins[i].result = gpio_request(pins[i].pin,pins[i].name);
                  pr_info("pins[i].result: %d\n",pins[i].result);
    		  if (pins[i].result != 0)
      			got_pins = 0;
                } else {
                pr_info("gpio is invalid\n");
                got_pins = 0;
                }
  	}

  	/* On any failures, return any pins we managed to get and quit. */
	if (!got_pins) {
    		FreePins();
                pr_warn("Could not get pins \n");
		return -EBUSY;
	}


 	/* Set port direction.  We assume we can do this if we got the pins.
  	   Set initial values to low (0v).
        */
  	for (i=0;i<PINS_NEEDED;i++)
  	{
    		gpio_direction_output(pins[i].pin,0);
  	}

        pr_info("\nSucceeded in registering character device %s\n",
               MYDEV_NAME);

        gpio_set_value(HD_RS, 0);
        msleep(1000);
        gpio_set_value(HD_RS, 1);
        msleep(1000);
        gpio_set_value(HD_RS, 0);
        return 0;
}

static void __exit gpio_test_exit(void)
{
	FreePins();
        misc_deregister(&my_misc_device);
        pr_info("\ndevice unregistered\n");
}
module_init(gpio_test_init);
module_exit(gpio_test_exit);
#endif

#ifdef CONFIG_OF
static const struct of_device_id hd44780_dt_match[] = {
        { .compatible = "gpio-hd44780" },
        { }
};
MODULE_DEVICE_TABLE(of, hd44780_dt_match);
#endif

#if 0
static struct hd44780_platform_data *
hd44780_parse_dt(struct device *dev)
{
        struct hd44780_platform_data *pdata;
        struct device_node *np = dev->of_node;
        unsigned int *gpios;
        int i, nrow;
 
         if (!np) {
                 dev_err(dev, "device lacks DT data\n");
                 return ERR_PTR(-ENODEV);
         }
 
         pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);
         if (!pdata) {
                 dev_err(dev, "could not allocate memory for platform data\n");
                 return ERR_PTR(-ENOMEM);
         }
 
         pdata->num_row_gpios = nrow = of_gpio_named_count(np, "row-gpios");
         if (nrow <= 0 ) {
                 dev_err(dev, "number of keypad rows/columns not specified\n");
                 return ERR_PTR(-EINVAL);
         }
 
         if (of_get_property(np, "gpio-activelow", NULL))
                 pdata->active_low = true;
 
         gpios = devm_kzalloc(dev,
                              sizeof(unsigned int) *
                                 (pdata->num_row_gpios),
                              GFP_KERNEL);
         if (!gpios) {
                 dev_err(dev, "could not allocate memory for gpios\n");
                 return ERR_PTR(-ENOMEM);
         }
 
         for (i = 0; i < pdata->num_row_gpios; i++)
                 gpios[i] = of_get_named_gpio(np, "row-gpios", i);
 
         pdata->row_gpios = gpios;
         return pdata;
}

static int hd44780_probe(struct platform_device *pdev)
{
         const struct hd44780_platform_data *pdata;
 
         pdata = dev_get_platdata(&pdev->dev);
         if (!pdata) {
                 pdata = hd44780_parse_dt(&pdev->dev);
                 if (IS_ERR(pdata)) {
                         dev_err(&pdev->dev, "no platform data defined\n");
                         return PTR_ERR(pdata);
                 }
         } else {
                 dev_err(&pdev->dev, "no pdata defined\n");
                 return -EINVAL;
         }
/*
        err = matrix_keypad_init_gpio(pdev, keypad);
        if (err)
                 goto err_free_mem;
*/
}
static int hd44780_remove(struct platform_device *pdev)
{
         struct matrix_keypad *keypad = platform_get_drvdata(pdev);

         device_init_wakeup(&pdev->dev, 0);

         //matrix_keypad_free_gpio(keypad);
         FreePins();
         kfree(keypad);

         return 0;
}

static struct platform_driver hd44780_driver = {
         .probe          = hd44780_probe,
         .remove         = hd44780_remove,
         .driver         = {
                 .name   = "hd44780",
                 .owner  = THIS_MODULE,
/*                 .pm     = &hd44780_pm_ops, */
                 .of_match_table = of_match_ptr(hd44780_dt_match),
         },
};
module_platform_driver(hd44780_driver);
#endif

MODULE_AUTHOR("Robert Berger");
MODULE_LICENSE("Dual BSD/GPL");	/* we don't wanna taint the kernel */
MODULE_VERSION("1:1.0");
MODULE_DESCRIPTION("HD44790 driver");
