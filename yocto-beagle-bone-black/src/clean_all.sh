source ../../../beagle-bone-io_slides/common/config.sh
echo "+TARGET_PLATFORM: ${TARGET_PLATFORM}"

if [ "${TARGET_PLATFORM}" = "yocto-cortexa8hf-beagle-bone-black" ]; then
         export DEVKIT="cortexa8hf"
         echo "+ DEVKIT=cortexa8hf"
elif [ "${TARGET_PLATFORM}" = "yocto-armv7a-beagle-bone-black" ]; then
         export DEVKIT="armv7a"
         echo "+ DEVKIT=armv7a"
else
  echo "+ TARGET_PLATFORM undefined"
fi

source ../env.sh

for dir in *; do
   if [ -d $dir ]; then
      echo cd $dir
      cd $dir
        if [ -f 10_clean.sh ]; then
        echo ./10_clean.sh
        ./10_clean.sh
        else
        echo make clean
        make clean
        fi
        if [ -d for-slides ]; then
         rm -rf for-slides
        fi
      cd ..
   fi
done

#git checkout 29_great_code/greatcode-for-gnu-linux/Sources/GC.cfg
#git checkout 29_great_code/greatcode-for-gnu-linux/Sources/gcode-1.140-p1
