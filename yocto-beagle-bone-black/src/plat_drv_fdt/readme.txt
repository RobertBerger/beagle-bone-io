root@beaglebone:~# ls /proc/device-tree/hd44780_gpio\@0/
col-gpios  compatible  gpio-activelow  name
root@beaglebone:~# 

root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/compatible
00000000  67 70 69 6f 2d 68 64 34  34 37 38 30 00           |gpio-hd44780.|
0000000d
root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/          
col-gpios       compatible      gpio-activelow  name            
root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/col-gpios 
00000000  00 00 00 41 00 00 00 1c  00 00 00 00 00 00 00 41  |...A...........A|
00000010  00 00 00 11 00 00 00 00                           |........|
00000018
root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/         
col-gpios       compatible      gpio-activelow  name            
root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/gpio-activelow 
root@beaglebone:~# hexdump -C /proc/device-tree/hd44780_gpio\@0/name           
00000000  68 64 34 34 37 38 30 5f  67 70 69 6f 00           |hd44780_gpio.|
0000000d

