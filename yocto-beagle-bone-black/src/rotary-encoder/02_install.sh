OLD_PATH=$PATH
PWD=`pwd`

source ../../env.sh

echo "+ sudo mkdir -p $NFS_EXPORTED_ROOTFS_ROOT$(pwd)"
sudo mkdir -p $NFS_EXPORTED_ROOTFS_ROOT$(pwd)
echo "+ sudo chown -R $USER:$USER $NFS_EXPORTED_ROOTFS_ROOT$(pwd)"
sudo chown -R $USER:$USER $NFS_EXPORTED_ROOTFS_ROOT$(pwd)
echo "+ cp -r * $NFS_EXPORTED_ROOTFS_ROOT$(pwd)"
cp -r * $NFS_EXPORTED_ROOTFS_ROOT$(pwd)

#echo "+sudo cp rotary_encoder.ko ${NFS_EXPORTED_ROOTFS_ROOT}/lib/modules/${KERNEL_MAINLINE_VER_ON_TARGET}-${CUSTOM_EXTRAVER}-${USER}+/kernel/drivers/input/misc"
#sudo cp rotary_encoder.ko ${NFS_EXPORTED_ROOTFS_ROOT}/lib/modules/${KERNEL_MAINLINE_VER_ON_TARGET}-${CUSTOM_EXTRAVER}-${USER}+/kernel/drivers/input/misc

echo "+ sudo cp rotary_encoder.ko ${NFS_EXPORTED_ROOTFS_ROOT}/lib/modules/${KERNEL_MAINLINE_VER:1}-${CUSTOM_EXTRAVER}-${USER}+/kernel/drivers/input/misc"
sudo cp rotary_encoder.ko ${NFS_EXPORTED_ROOTFS_ROOT}/lib/modules/${KERNEL_MAINLINE_VER:1}-${CUSTOM_EXTRAVER}-${USER}+/kernel/drivers/input/misc

PATH=$OLD_PATH
cd $PWD
