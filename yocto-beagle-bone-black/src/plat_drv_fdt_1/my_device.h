typedef struct PinSet {
  int pin;      /* Linux GPIO pin number */
  char* name;   /* Name of the pin, supplied to gpio_request() */
  int result;   /* set to zero on successfully obtaining pin */
} tPinSet;

static tPinSet pins[] = {
  {-1,  "HD44780_RW",  -1},
  {-2,  "HD44780_RS",  -1},
  {-3, "HD44780_DB4", -1},
  {-4, "HD44780_DB5", -1},
  {-5, "HD44780_DB6", -1},
  {-6, "HD44780_DB7", -1},
};

struct my_device {
         struct platform_device *pdev;
         struct list_head        node;
#if 0 
         struct clk      *clk;
        void __iomem    *mmio_base;
         unsigned long   phys_base;
 
         const char      *label;
         int             port_id;
         int             type;
         int             use_count;
        int             irq;
         int             drcmr_rx;
         int             drcmr_tx;
#endif
         unsigned int *gpios;
//         int ngpios_fdt;
         struct device_node      *of_node;
};

static int ngpios_fdt;
