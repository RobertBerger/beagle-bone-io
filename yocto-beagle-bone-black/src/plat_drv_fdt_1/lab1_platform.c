/*
n is: Copyright the Linux Foundation, 2012
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * The primary maintainer for this code is Jerry Cooperstein
 * The CONTRIBUTORS file (distributed with this
 * file) lists those known to have contributed to the source.
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/*
 * Basic Platform Driver Skeleton
 *
 * Write a very basic platform driver, containing only init() and
 * exit() functions, and dummy probe() and remove() functions.
 *
 * Using the driver you write or the skeleton one provided, load it
 * and check under /sys/bus/platform to make sure the driver is
 * showing up.
 *
@*/

/*
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
*/

#define pr_fmt(fmt) KBUILD_MODNAME " %s:%d: " fmt, __func__, __LINE__


#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/slab.h>         /* kmalloc */
#include <linux/miscdevice.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include "my_device.h"
#include "platform_data.h"

#define DRV_NAME "my_platform_drv"
#define MYDEV_NAME "my_platform_dev"

#if 0
static void print_device_tree_node(struct device_node *node, int depth) {
  int i = 0;
  struct device_node *child;
  struct property    *properties;
  char                indent[255] = "";

  for(i = 0; i < depth * 3; i++) {
    indent[i] = ' ';
  }
  indent[i] = '\0';
  ++depth;

  for_each_child_of_node(node, child) {
    printk(KERN_INFO "%s{ name = %s\n", indent, child->name);
    printk(KERN_INFO "%s  type = %s\n", indent, child->type);
    for (properties = child->properties; properties != NULL; properties = properties->next) {
      printk(KERN_INFO "%s  %s (%d)\n", indent, properties->name, properties->length);
    }
    print_device_tree_node(child, depth);
    printk(KERN_INFO "%s}\n", indent);
  }
}
#endif

static int mycdrv_open(struct inode *inode, struct file *file)
{
        pr_info(" attempting to open device: %s:\n", MYDEV_NAME);
        pr_info(" MAJOR number = %d, MINOR number = %d\n",
               imajor(inode), iminor(inode));
        pr_info(" successfully open  device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("First time open: Allocate resources\n");
        return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
        pr_info(" CLOSING device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("Last time close: Deallocate resources\n");

        return 0;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .open = mycdrv_open,
        .release = mycdrv_release
};

static struct miscdevice my_misc_device = {
        .minor = MISC_DYNAMIC_MINOR,
        .name = MYDEV_NAME,
        .fops = &mycdrv_fops,
};

static struct platform_device *my_platform_device;

static struct of_device_id hd44780_of_match[] = {
  { .compatible = "gpio-hd44780", },
  {}
};

MODULE_DEVICE_TABLE(of, hd44780_of_match);

static struct my_device_platform_data *
my_device_parse_dt(struct device *dev)
{
         struct my_device_platform_data *pdata;
         struct device_node *np = dev->of_node;
         unsigned int *gpios;
         int i, ncol;

#if 0
	print_device_tree_node(of_find_node_by_path("/"), 0);
#endif
 
         if (!np) {
                 dev_err(dev, "device lacks DT data\n");
                 return ERR_PTR(-ENODEV);
         }

         pdata = devm_kzalloc(dev, sizeof(*pdata), GFP_KERNEL);
         if (!pdata) {
                 dev_err(dev, "could not allocate memory for platform data\n");
                 return ERR_PTR(-ENOMEM);
         }
 
         pdata->num_col_gpios = ncol = of_gpio_named_count(np, "col-gpios");
         if (ncol <= 0) {
                 dev_err(dev, "number of col-gpios not specified\n");
                 return ERR_PTR(-EINVAL);
         }
 
         if (of_get_property(np, "gpio-activelow", NULL))
                 pdata->active_low = true;
 
         gpios = devm_kzalloc(dev,
                              sizeof(unsigned int) *
                                 (pdata->num_col_gpios),
                              GFP_KERNEL);
         if (!gpios) {
                 dev_err(dev, "could not allocate memory for gpios\n");
                 return ERR_PTR(-ENOMEM);
         }
 
         for (i = 0; i < pdata->num_col_gpios; i++)
                 gpios[i] = of_get_named_gpio(np, "col-gpios", i);
 
         pdata->col_gpios = gpios;
 
         return pdata;
}


#if 0
static int __init my_platform_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	dev_dbg(dev, "probe\n");
        pr_info("Hit my_platform_probe\n");
        return 0;
}
#endif

#if 0
static int __init my_platform_probe(struct platform_device *pdev)
{
         int ret;
         int ncol;
         struct device_node *node;
         node = pdev->of_node;

         const struct of_device_id *match;
         match = of_match_device(hd44780_of_match, &pdev->dev);

         pr_info("match: %d\n",match);

         if (!match)
            return -EINVAL;       

         ncol = of_gpio_named_count(&pdev->dev.node, "col-gpios");
         pr_info("ncol: %d\n",ncol);

         const struct my_device_platform_data *pdata;

         pdata = my_device_parse_dt(&pdev->dev);
                 if (IS_ERR(pdata)) {
                         dev_err(&pdev->dev, "no platform data defined\n");
                         return PTR_ERR(pdata);
                 }
         ret = misc_register(&my_misc_device);
         if (ret != 0) {
                 pr_err("cannot register miscdev \n");
                 goto out;
         }
         pr_info("initialized.\n");
 
         return 0;
out:
         return ret;
}
#endif

/* return any acquired pins. */
static void FreePins(void)
{
  int i;
  for (i=0;i<ngpios_fdt;i++)
  {
    if (pins[i].result == 0)
    {
      gpio_free(pins[i].pin);
      pins[i].result = -1;    /* defensive programming - avoid multiple free. */
    }
  }
}

static int __init my_platform_probe(struct platform_device *pdev)
{
         struct my_device *my;
         struct device *dev = &pdev->dev;
         struct device_node *node;
         int ret,ncol,i;
         int got_pins = 1;
         /* unsigned int *gpios; */
         //int ngpios_fdt, rs;

         /* --> it's called twize - protection */
         static int first_time=1;

         if (first_time == 0) {	
               return 0;
         }

	first_time=0;
         /* <-- it's called twize - protection */

	 /* --> register misc char device */
         ret = misc_register(&my_misc_device);
         if (ret != 0) {
                 pr_err("cannot register miscdev \n");
                goto out;
         }
         pr_info("initialized.\n");
         /* <-- register misc char device */
         

         my = devm_kzalloc(dev, sizeof(struct my_device), GFP_KERNEL);
         if (my == NULL)
                 return -ENOMEM;

         node = dev->of_node;

         if (node) {
           pr_err("good \n");

           /* --> read rs gpio from fdt */

           ngpios_fdt = of_gpio_named_count(node, "rs");

           if (ngpios_fdt != 1) {
                 dev_err(dev, "rs not defined in fdt\n");
                 return -EINVAL;
           }  
           pins[0].pin=of_get_named_gpio(node, "rs", 0);

	   /* <-- read rs gpio from fdt */

           /* --> read rw gpio from fdt */

           ngpios_fdt += of_gpio_named_count(node, "rw");

           if (ngpios_fdt != 2) {
                 dev_err(dev, "rw not defined in fdt\n");
                 return -EINVAL;
           } 
           pins[1].pin=of_get_named_gpio(node, "rw", 0);

           /* <-- read rw gpio from fdt */


           /* --> read db4 gpio from fdt */

           ngpios_fdt += of_gpio_named_count(node, "db4");

           if (ngpios_fdt != 3) {
                 dev_err(dev, "db4 not defined in fdt\n");
                 return -EINVAL;
           }
           pins[2].pin=of_get_named_gpio(node, "db4", 0);

           /* <-- read db4 gpio from fdt */

           /* --> read db5 gpio from fdt */

           ngpios_fdt += of_gpio_named_count(node, "db5");

           if (ngpios_fdt != 4) {
                 dev_err(dev, "db5 not defined in fdt\n");
                 return -EINVAL;
           } 
           pins[3].pin=of_get_named_gpio(node, "db5", 0);

           /* <-- read db5 gpio from fdt */

           /* --> read db6 gpio from fdt */

           ngpios_fdt += of_gpio_named_count(node, "db6");

           if (ngpios_fdt != 5) {
                 dev_err(dev, "db6 not defined in fdt\n");
                 return -EINVAL;
           } 
           pins[4].pin=of_get_named_gpio(node, "db6", 0);

           /* <-- read db6 gpio from fdt */

           /* --> read db7 gpio from fdt */

           ngpios_fdt += of_gpio_named_count(node, "db7");

           if (ngpios_fdt != 6) {
                 dev_err(dev, "db7 not defined in fdt\n");
                 return -EINVAL;
           } 
           pins[5].pin=of_get_named_gpio(node, "db7", 0);

           /* <-- read db7 gpio from fdt */

           pr_info("ngpios_fdt %d \n",ngpios_fdt);

           /* Request pins */
        for (i=0;i<ngpios_fdt;i++) {
                if (gpio_is_valid(pins[i].pin)) {
                  pins[i].result = gpio_request(pins[i].pin,pins[i].name);
                  pr_info("pins[%d].pin,pins[%d].name,pins[%d].result: %d %s %d\n",i,i,i,pins[i].pin,pins[i].name,pins[i].result);
                  if (pins[i].result != 0)
                        got_pins = 0;
                } else {
                pr_info("gpio is invalid\n");
                got_pins = 0;
                }
        }

        /* On any failures, return any pins we managed to get and quit. */
        if (!got_pins) {
                FreePins();
                pr_warn("Could not get pins \n");
                return -EBUSY;
        }

        /* Set port direction.  We assume we can do this if we got the pins.
           Set initial values to low (0v).
		*/
		for (i=0;i<ngpios_fdt;i++)
        {
                gpio_direction_output(pins[i].pin,0);
        }



#if 0
           ncol = of_gpio_named_count(node, "col-gpios");
           pr_info("pin count %d \n", ncol);

           if (ncol != 6) {
                 dev_err(dev, "we'll need 6 gpio pins here\n");
                 return -EINVAL;
           }

          my->gpios = devm_kzalloc(dev, sizeof(unsigned int) * ncol, GFP_KERNEL);
          if (!my->gpios) {
                 dev_err(dev, "could not allocate memory for gpios\n");
                 return -ENOMEM;
          }

          for (i = 0; i < ncol; i++) {
                 my->gpios[i] = of_get_named_gpio(node, "col-gpios", i);
                 pr_info("gpios[%d]: %d\n",i,my->gpios[i]);
          }
 
#endif
         } else {
           pr_err("bad\n");
         }

         pr_info("initialized\n");
         return 0;

out:
        return ret;

}

static int __exit my_platform_remove(struct platform_device *pdev)
{
         /* --> it's called twize - protection */
         static int first_time=1;

         if (first_time == 0) {
               return 0;
         }

        first_time=0;
        /* <-- it's called twize - protection */

        pr_info("Hit my_platform_remove\n");
        FreePins();
	misc_deregister(&my_misc_device);
        return 0;
}

#if 0
static const struct platform_device_id my_platform_id_table[] = {
        {"my_platform_1", 0},
        {"my_platform_2", 0},
        {},
};

MODULE_DEVICE_TABLE(platform, my_platform_id_table);
#endif

#if 0
static int __init my_init(void)
{
        pr_info("Loading my platform device\n");
        return platform_driver_register(&my_platform_driver);
}
#endif

#if 0
static struct of_device_id hd44780_of_match[] = {
  { .compatible = "gpio-hd44780", },
  {}
};

MODULE_DEVICE_TABLE(of, hd44780_of_match);
#endif
static struct platform_driver my_platform_driver = {
/*        .probe = my_platform_probe, */
         .remove = __exit_p(my_platform_remove),
         .driver = {
                   .name = DRV_NAME,
                   .owner = THIS_MODULE,
                   .of_match_table = of_match_ptr(hd44780_of_match),
                   },
/*        .id_table = my_platform_id_table, */
};
/*module_platform_driver(my_device_driver);*/

static int __init my_init(void)
{
         int err;
 
         pr_info("my_platform driver is initialising\n");
 
         my_platform_device = platform_device_register_simple(DRV_NAME, -1, NULL, 0);
         if (IS_ERR(my_platform_device)) {
                 pr_err("my_platform_device failed to initialize\n");
                 return PTR_ERR(my_platform_device);
         }
 
         err = platform_driver_probe(&my_platform_driver, my_platform_probe);
         if (err) {
                 pr_err("err: %d\n",err);
                 goto unreg_platform_device;
         }
         pr_info("my_platform driver initialized\n");
         return 0;
 
unreg_platform_device:
         pr_err("my_platform_driver_probe failed\n");
         platform_device_unregister(my_platform_device);
         platform_driver_unregister(&my_platform_driver);
         return err;
}

static void __exit my_exit(void)
{
        pr_info("Unloading my platform device\n");
        platform_device_unregister(my_platform_device);
        platform_driver_unregister(&my_platform_driver);
        pr_info("platform device and driver unloaded\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Jerry Cooperstein");
MODULE_DESCRIPTION("LF331:1.8 s_21/lab1_platform.c");
MODULE_LICENSE("GPL v2");

