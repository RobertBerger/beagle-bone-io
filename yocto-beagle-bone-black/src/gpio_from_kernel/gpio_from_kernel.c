/*
 * HD44780 LCD/GPIO driver for Linux
 *  Robert Berger <robert.berger@reliableembeddedsystems.com>
 *
 *  adapted from hd44780 driver using gpio pins -  bifferos@yahoo.co.uk, 2008
 *
 */

#include <linux/module.h>       /* for modules */
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/init.h>         /* module_init, module_exit */
#include <linux/slab.h>         /* kmalloc */
#include <linux/miscdevice.h>
#include <linux/gpio.h>
#if 0
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include "gpio_from_kernel.h"
#endif
#include <linux/delay.h>

#define MYDEV_NAME "gpio_test_dev"

/* note that those pins need to initialized to be GPIOs in the fdt */

/*  Pin ID     Linux GPIO number      Beagle Bone Black pin annotation */
#define HD_RS      49 /*30*/                 /* traffic_leds_s0    p9_11 gpio Bank0.Pin30 PIN#30 */        
/* #define HD_RW      14                 not needed - always write active */
/*
#define HD_E       15              
#define HD_DB4     17              
#define HD_DB5     18              
#define HD_DB6     20              
#define HD_DB7     21              
*/


/* Structures to hold the pins we've successfully allocated. */
typedef struct PinSet {
  int pin;      /* Linux GPIO pin number */
  char* name;   /* Name of the pin, supplied to gpio_request() */
  int result;   /* set to zero on successfully obtaining pin */
} tPinSet;


static tPinSet pins[] = {
  {HD_RS,  "HD44780_RS",  -1},
};

#define PINS_NEEDED (sizeof(pins)/sizeof(tPinSet))

// Macros for setting control lines
#define HD_RS_LOW  gpio_set_value(HD_RS, 0);
#define HD_RS_HIGH gpio_set_value(HD_RS, 1);
#define HD_E_LOW   gpio_set_value(HD_E, 0);
#define HD_E_HIGH  gpio_set_value(HD_E, 1);

/* Buffer to store data */
static char*    memory_buffer;

/* return any acquired pins. */
static void FreePins(void)
{
  int i;
  for (i=0;i<PINS_NEEDED;i++)
  {
    if (pins[i].result == 0)
    {
      gpio_free(pins[i].pin);
      pins[i].result = -1;    /* defensive programming - avoid multiple free. */
    }
  }
}


static int mycdrv_open(struct inode *inode, struct file *file)
{
        pr_info(" attempting to open device: %s:\n", MYDEV_NAME);
        pr_info(" MAJOR number = %d, MINOR number = %d\n",
               imajor(inode), iminor(inode));
        pr_info(" successfully open  device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("First time open: Allocate resources\n");
        return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
        pr_info(" CLOSING device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("Last time close: Deallocate resources\n");

        return 0;
}

static ssize_t
mycdrv_write(struct file *file, const char __user *buf, size_t lbuf,
             loff_t *ppos)
{
    int result;

    if (!lbuf)
    {
        return (0);
    }

    result=copy_from_user (memory_buffer, buf, 1);
    /* pr_info("Written: %c\n",memory_buffer[0]); */
    if (memory_buffer[0]=='0') {
       pr_info("0 was written - clearing gpio\n");
       gpio_set_value(HD_RS, 0);
    } else if (memory_buffer[0]=='1') {
       pr_info("1 was written - setting gpio\n");
       gpio_set_value(HD_RS, 1);
    }
    return (1);
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .write = mycdrv_write,
        .open = mycdrv_open,
        .release = mycdrv_release
};

static struct miscdevice my_misc_device = {
        .minor = MISC_DYNAMIC_MINOR,
        .name = MYDEV_NAME,
        .fops = &mycdrv_fops,
};

static int __init gpio_test_init(void)
{
	int i;
	int got_pins = 1;

        if (misc_register(&my_misc_device)) {
                pr_warn("Couldn't register device misc, %d.\n",
                my_misc_device.minor);
                return -EBUSY;
        }

            /* Allocating memory for the buffer */
        memory_buffer = kmalloc (1, GFP_KERNEL);
       if (!memory_buffer)
       {
        return -ENOMEM;
       }

       memset (memory_buffer, 0, 1);

        pr_info("PINS_NEEDED %d\n",PINS_NEEDED);

	/* Request pins */
  	for (i=0;i<PINS_NEEDED;i++) {
                if (gpio_is_valid(pins[i].pin)) {
    		  pins[i].result = gpio_request(pins[i].pin,pins[i].name);
                  pr_info("pins[i].result: %d\n",pins[i].result);
    		  if (pins[i].result != 0)
      			got_pins = 0;
                } else {
                pr_info("gpio is invalid\n");
                got_pins = 0;
                }
  	}

  	/* On any failures, return any pins we managed to get and quit. */
	if (!got_pins) {
    		FreePins();
                pr_warn("Could not get pins \n");
		return -EBUSY;
	}

 	/* Set port direction.  We assume we can do this if we got the pins.
  	   Set initial values to low (0v).
        */
  	for (i=0;i<PINS_NEEDED;i++)
  	{
    		gpio_direction_output(pins[i].pin,0);
  	}

        pr_info("\nSucceeded in registering character device %s\n",
               MYDEV_NAME);
/*
        gpio_set_value(HD_RS, 0);
        msleep(1000);
        gpio_set_value(HD_RS, 1);
        msleep(1000);
        gpio_set_value(HD_RS, 0);
*/
        return 0;
}

static void __exit gpio_test_exit(void)
{
	FreePins();
        misc_deregister(&my_misc_device);
        pr_info("\ndevice unregistered\n");
}

module_init(gpio_test_init);
module_exit(gpio_test_exit);

MODULE_AUTHOR("Robert Berger");
MODULE_LICENSE("Dual BSD/GPL");	/* we don't wanna taint the kernel */
MODULE_VERSION("1:1.0");
MODULE_DESCRIPTION("HD44790 driver");
