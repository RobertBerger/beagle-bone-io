MODULE_NAME=gpio_from_kernel

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo 
fi

echo "+ $SUDO insmod $MODULE_NAME.ko"
$SUDO insmod $MODULE_NAME.ko

echo "+ $SUDO lsmod | grep $MODULE_NAME"
$SUDO lsmod | grep $MODULE_NAME

echo "+ $SUDO rmmod $MODULE_NAME.ko"
$SUDO rmmod $MODULE_NAME.ko

echo "+ $SUDO lsmod | grep $MODULE_NAME"
$SUDO lsmod | grep $MODULE_NAME

echo "+ $SUDO dmesg | tail -n 10"
$SUDO dmesg | tail -n 10
