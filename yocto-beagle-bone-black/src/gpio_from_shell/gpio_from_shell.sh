# --> make pin number x an output

echo "+ ls -la /sys/class/gpio"
ls -la /sys/class/gpio
echo "+ GPIO number:"
read pin_number
echo "+ echo ${pin_number} > /sys/class/gpio/export"
echo ${pin_number} > /sys/class/gpio/export
echo "+ ls -la /sys/class/gpio"
ls -la /sys/class/gpio
echo "+ echo \"out\" > /sys/class/gpio/gpio${pin_number}/direction"
echo "out" > /sys/class/gpio/gpio${pin_number}/direction

# <--  make pin number x an output

# --> toggle the pin 

echo "+ ls -la /sys/class/gpio/gpio${pin_number}"
ls -la /sys/class/gpio/gpio${pin_number}
echo "+ echo 0 > /sys/class/gpio/gpio${pin_number}/value"
echo 0 > /sys/class/gpio/gpio${pin_number}/value
echo "+ sleep 5"
sleep 5
echo "+ echo 1 > /sys/class/gpio/gpio${pin_number}/value"
echo 1 > /sys/class/gpio/gpio${pin_number}/value
echo "+ sleep 5"
sleep 5

# <-- toggle the pin
