/*
n is: Copyright the Linux Foundation, 2012
 *
 * This Copyright is retained for the purpose of protecting free
 * redistribution of source.
 *
 *     URL:    http://training.linuxfoundation.org
 *     email:  trainingquestions@linuxfoundation.org
 *
 * The primary maintainer for this code is Jerry Cooperstein
 * The CONTRIBUTORS file (distributed with this
 * file) lists those known to have contributed to the source.
 *
 * This code is distributed under Version 2 of the GNU General Public
 * License, which you should have received with the source.
 *
 */
/*
 * Basic Platform Driver Skeleton
 *
 * Write a very basic platform driver, containing only init() and
 * exit() functions, and dummy probe() and remove() functions.
 *
 * Using the driver you write or the skeleton one provided, load it
 * and check under /sys/bus/platform to make sure the driver is
 * showing up.
 *
@*/

/*
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
*/

#define pr_fmt(fmt) KBUILD_MODNAME " %s:%d: " fmt, __func__, __LINE__


#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fs.h>           /* file_operations */
#include <linux/uaccess.h>      /* copy_(to,from)_user */
#include <linux/slab.h>         /* kmalloc */
#include <linux/miscdevice.h>

#define DRV_NAME "my_platform_drv"
#define MYDEV_NAME "my_platform_dev"

static int mycdrv_open(struct inode *inode, struct file *file)
{
        pr_info(" attempting to open device: %s:\n", MYDEV_NAME);
        pr_info(" MAJOR number = %d, MINOR number = %d\n",
               imajor(inode), iminor(inode));
        pr_info(" successfully open  device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("First time open: Allocate resources\n");
        return 0;
}

static int mycdrv_release(struct inode *inode, struct file *file)
{
        pr_info(" CLOSING device: %s:\n\n", MYDEV_NAME);
        pr_info("ref=%lu\n", module_refcount(THIS_MODULE));
        if (module_refcount(THIS_MODULE) == 1)
                pr_info("Last time close: Deallocate resources\n");

        return 0;
}

static const struct file_operations mycdrv_fops = {
        .owner = THIS_MODULE,
        .open = mycdrv_open,
        .release = mycdrv_release
};

static struct miscdevice my_misc_device = {
        .minor = MISC_DYNAMIC_MINOR,
        .name = MYDEV_NAME,
        .fops = &mycdrv_fops,
};

static struct platform_device *my_platform_device;

#if 0
static int __init my_platform_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	dev_dbg(dev, "probe\n");
        pr_info("Hit my_platform_probe\n");
        return 0;
}
#endif
#if 1
static int __init my_platform_probe(struct platform_device *dev)
{
         int ret;
 
         ret = misc_register(&my_misc_device);
         if (ret != 0) {
                 pr_err("cannot register miscdev \n");
                 goto out;
         }
         pr_info("initialized.\n");
 
         return 0;
out:
         return ret;
}
#endif

static int __exit my_platform_remove(struct platform_device *pdev)
{
        pr_info("Hit my_platform_remove\n");
	misc_deregister(&my_misc_device);
        return 0;
}

static const struct platform_device_id my_platform_id_table[] = {
        {"my_platform_1", 0},
        {"my_platform_2", 0},
        {},
};

MODULE_DEVICE_TABLE(platform, my_platform_id_table);

static struct platform_driver my_platform_driver = {
        .driver = {
                   .name = DRV_NAME,
                   .owner = THIS_MODULE,
                   },
/*        .probe = my_platform_probe, */
        .remove = __exit_p(my_platform_remove),
/*        .id_table = my_platform_id_table, */
};
#if 0
static int __init my_init(void)
{
        pr_info("Loading my platform device\n");
        return platform_driver_register(&my_platform_driver);
}
#endif

static int __init my_init(void)
{
         int err;
 
         pr_info("my_platform driver is initialising\n");
 
         my_platform_device = platform_device_register_simple(DRV_NAME, -1, NULL, 0);
         if (IS_ERR(my_platform_device)) {
                 pr_err("my_platform_device failed to initialize\n");
                 return PTR_ERR(my_platform_device);
         }
 
         err = platform_driver_probe(&my_platform_driver, my_platform_probe);
         if (err) {
                 pr_err("err: %d\n",err);
                 goto unreg_platform_device;
         }
         pr_info("my_platform driver initialized\n");
         return 0;
 
unreg_platform_device:
         pr_err("my_platform_driver_probe failed\n");
         platform_device_unregister(my_platform_device);
         return err;
}

static void __exit my_exit(void)
{
        pr_info("Unloading my platform device\n");
        platform_device_unregister(my_platform_device);
        platform_driver_unregister(&my_platform_driver);
        pr_info("platform device and driver unloaded\n");
}

module_init(my_init);
module_exit(my_exit);

MODULE_AUTHOR("Jerry Cooperstein");
MODULE_DESCRIPTION("LF331:1.8 s_21/lab1_platform.c");
MODULE_LICENSE("GPL v2");

