clear

echo "+ cpupower frequency-info"
cpupower frequency-info

echo "+ cpupower frequency-set -g powersave"
cpupower frequency-set -g powersave
echo "+ cpupower frequency-info"
cpupower frequency-info

echo "+ cpupower frequency-set -g performance"
cpupower frequency-set -g performance
echo "+ cpupower frequency-info"
cpupower frequency-info

echo "+ cpupower frequency-set -g ondemand"
cpupower frequency-set -g ondemand
echo "+ cpupower frequency-info"
cpupower frequency-info

echo "+ sleep 5"
sleep 5

echo "+ cpupower frequency-info"
cpupower frequency-info


