#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <linux/input.h>
#include <string.h>
#include <stdio.h>

static const char *const evval[3] = {
	"RELEASED",
	"PRESSED ",
	"REPEATED"
};

#define MYLED "/sys/class/leds/led:purple"

FILE *f;

void init_led(void)
{
	f = fopen(MYLED "/trigger", "w");

	if (f == NULL)
		exit(EXIT_FAILURE);

	fprintf(f, "none");
	fclose(f);

}

int set_led(int i)
{
	char str[1];

	f = fopen(MYLED "/brightness", "w");

	if (f == NULL)
		exit(EXIT_FAILURE);

	sprintf(str, "%d", i);
	fprintf(f, "%s", str);

	fclose(f);

	return EXIT_SUCCESS;
}

int main(void)
{
	const char *dev = "/dev/input/by-path/platform-gpio_keys-event";
	struct input_event ev;
	ssize_t n;
	int fd;

	init_led();
	set_led(0);

	fd = open(dev, O_RDONLY);
	if (fd == -1) {
		fprintf(stderr, "Cannot open %s: %s.\n", dev, strerror(errno));
		return EXIT_FAILURE;
	}

	while (1) {
		n = read(fd, &ev, sizeof ev);
		if (n == (ssize_t) - 1) {
			if (errno == EINTR)
				continue;
			else
				break;
		} else if (n != sizeof ev) {
			errno = EIO;
			break;
		}

		if (ev.type == EV_KEY && ev.value >= 0 && ev.value <= 2) {
			printf("%s 0x%04x (%d)\n", evval[ev.value],
			       (int)ev.code, (int)ev.code);
			if (strcmp(evval[ev.value], evval[1]) == 0) {
				printf("PRESSED/LED on\n");
				set_led(1);
			} else {
				printf("RELEASED/LED off.\n");
				set_led(0);
			}
		}

	}
	fflush(stdout);
	fprintf(stderr, "%s.\n", strerror(errno));
	return EXIT_FAILURE;
}
