#include <stdlib.h>
#include <stdio.h>

#define MYLED "/sys/class/leds/led:purple"

FILE *f;

int main(int argc, char *argv[])
{
	argc = argc;
	argv = argv;

	f = fopen(MYLED "/trigger", "w");

	if (f == NULL)
		exit(EXIT_FAILURE);

	fprintf(f, "timer");
	fclose(f);

	f = fopen(MYLED "/delay_on", "w");

	if (f == NULL)
		exit(EXIT_FAILURE);

	fprintf(f, "1000");
	fclose(f);

	f = fopen(MYLED "/delay_off", "w");

	if (f == NULL)
		exit(EXIT_FAILURE);

	fprintf(f, "1000");
	fclose(f);

	return EXIT_SUCCESS;
}
