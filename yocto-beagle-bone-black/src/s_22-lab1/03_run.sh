echo "+ tree /proc/device-tree/fakedummy"
echo "+ press <ENTER> to go on"
read r

echo "+ tree /proc/device-tree/fakedummy"
tree /proc/device-tree/fakedummy

echo "+ cat /proc/device-tree/fakedummy/name | hexdump -C"
cat /proc/device-tree/fakedummy/name | hexdump -C
echo "+ cat /proc/device-tree/fakedummy/compatible | hexdump -C"
cat /proc/device-tree/fakedummy/compatible | hexdump -C
echo "+ cat /proc/device-tree/fakedummy/myproperty | hexdump -C"
cat /proc/device-tree/fakedummy/myproperty | hexdump -C
echo "+ cat /proc/device-tree/fakedummy/myotherprop | hexdump -C"
cat /proc/device-tree/fakedummy/myotherprop | hexdump -C
echo "+ cat /proc/device-tree/fakedummy/default-state | hexdump -C"
cat /proc/device-tree/fakedummy/default-state | hexdump -C

echo "+ cat lab1_my.dtsi"
cat lab1_my.dtsi
