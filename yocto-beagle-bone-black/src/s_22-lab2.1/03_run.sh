#source ../env.sh
MODULE_NAME_1=lab1_platform
#DEVICE_NAME=mycdrv
#USER_SPACE_READ=lab1_read
#USER_SPACE_WRITE=lab1_write

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo 
fi

echo "+ modinfo $MODULE_NAME_1.ko"
modinfo $MODULE_NAME_1.ko

#plus_echo "$SUDO rm -f /dev/$DEVICE_NAME"
#$SUDO rm -f /dev/$DEVICE_NAME

echo "+ $SUDO insmod $MODULE_NAME_1.ko"
$SUDO insmod $MODULE_NAME_1.ko

echo "+ sleep 1"
sleep 1

echo "+ ls -la /sys/bus/platform/drivers"
ls -la /sys/bus/platform/drivers

echo "+ ls -la /sys/bus/platform/drivers/my_platform"
ls -la /sys/bus/platform/drivers/my_platform

#echo "Press <ENTER> to go on"
#read r

echo "+$SUDO rmmod $MODULE_NAME_1.ko"
$SUDO rmmod $MODULE_NAME_1.ko

echo "+ lsmod | grep lab"
lsmod | grep lab

