HERE=`pwd`

source ../../env.sh
source local-config.mak

cp ${CUSTOM_KERNELDIR}/include/uapi/linux/input.h .
cp ${CUSTOM_KERNELDIR}/include/uapi/linux/uinput.h .

make clean
make

cd ${HERE}
