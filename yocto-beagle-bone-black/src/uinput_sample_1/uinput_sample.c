#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
/*#include "input.h"*/
#include "uinput.h"

#define FOO(...) printf(__VA_ARGS__)

#define die(msg) do { perror(msg); exit(EXIT_FAILURE); } while (0)

#if 0
#define die(str, printf(__VA_ARGS__)) do { \
        perror(str); \
        exit(EXIT_FAILURE); \
    } while(0)
#endif

int main(void)
{
	int fd;
	struct uinput_user_dev uidev;
	struct input_event ev;
	int dx, dy;
	int i;

	fd = open("/dev/uinput", O_WRONLY | O_NONBLOCK);
	if (fd < 0)
		die("error: open");

	if (ioctl(fd, UI_SET_EVBIT, EV_KEY) < 0)
		die("error: ioctl");
	if (ioctl(fd, UI_SET_KEYBIT, KEY_D) < 0)
		die("error: ioctl");

	memset(&uidev, 0, sizeof(uidev));
	snprintf(uidev.name, UINPUT_MAX_NAME_SIZE, "uinput-sample");
	uidev.id.bustype = BUS_USB;
	uidev.id.vendor = 0x1;
	uidev.id.product = 0x1;
	uidev.id.version = 1;

	/* write structure in to uinput file descriptor */

	if (write(fd, &uidev, sizeof(uidev)) < 0)
		die("error: write");

	/* create the device */

	if (ioctl(fd, UI_DEV_CREATE) < 0)
		die("error: ioctl");

	sleep(2);

	if (ioctl(fd, UI_DEV_DESTROY) < 0)
		die("error: ioctl");

	close(fd);

	return 0;
}
