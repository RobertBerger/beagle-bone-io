HERE=$(pwd)
set -x
cd evtest
./autogen.sh
make
make install
set +x
cd ${HERE}
