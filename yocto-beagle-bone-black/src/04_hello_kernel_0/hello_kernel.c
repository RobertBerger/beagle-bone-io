/* Module source file 'hello_kernel.c'. */

#include <linux/module.h>	/* for all modules */
#include <linux/init.h>		/* for entry/exit macros */
#include <linux/kernel.h>	/* for printk priority macros */

static int __init hello_init(void)
{
	pr_info("Hello Kernel Init\n");
	return 0;
}

static void __exit hello_exit(void)
{
	pr_info("Hello Kernel Exit\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_AUTHOR("Robert Berger");
MODULE_LICENSE("Dual BSD/GPL");	/* we don't wanna taint the kernel */
MODULE_VERSION("1:1.0");
MODULE_DESCRIPTION("Hello Kernel.");
