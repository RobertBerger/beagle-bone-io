MODULE_NAME=hello_kernel

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo 
fi

echo "+ $SUDO insmod $MODULE_NAME.ko"
$SUDO insmod $MODULE_NAME.ko

echo "+ $SUDO lsmod | grep $MODULE_NAME"
$SUDO lsmod | grep $MODULE_NAME

echo "+ $SUDO rmmod $MODULE_NAME.ko"
$SUDO rmmod $MODULE_NAME.ko

echo "+ $SUDO lsmod | grep $MODULE_NAME"
$SUDO lsmod | grep $MODULE_NAME

if [ ! -d "/lib/modules/`uname -r`/kernel/drivers" ]; then
  echo "+ mkdir /lib/modules/`uname -r`/kernel/drivers"
  mkdir /lib/modules/`uname -r`/kernel/drivers
fi

echo "+ cp hello_kernel.ko /lib/modules/`uname -r`/kernel/drivers"
cp hello_kernel.ko /lib/modules/`uname -r`/kernel/drivers

echo "+ depmod -a"
depmod -a

echo "+ cat /lib/modules/`uname -r`/modules.dep"
cat /lib/modules/`uname -r`/modules.dep

echo "+ modprobe hello_kernel"
modprobe hello_kernel

echo "+ modprobe -r hello_kernel"
modprobe -r hello_kernel
