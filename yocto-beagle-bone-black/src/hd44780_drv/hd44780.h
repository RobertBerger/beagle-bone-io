typedef struct PinSet {
	int pin;	/* Linux GPIO pin number */
	char *name;	/* Name of the pin, supplied to gpio_request() */
	int result;	/* set to zero on successfully obtaining pin */
} tPinSet;

static tPinSet pins[] = {
	{-1, "HD44780_E", -1},
	{-2, "HD44780_RS", -1},
	{-3, "HD44780_DB4", -1},
	{-4, "HD44780_DB5", -1},
	{-5, "HD44780_DB6", -1},
	{-6, "HD44780_DB7", -1},
};

/* number of GPIO pins read from fdt */
static int ngpios_fdt;
