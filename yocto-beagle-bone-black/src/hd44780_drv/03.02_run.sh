MODULE_NAME_1="hd44780_drv"

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo 
fi

echo "+ $SUDO rmmod $MODULE_NAME_1"
$SUDO rmmod $MODULE_NAME_1

echo "+ $SUDO insmod $MODULE_NAME_1.ko"
$SUDO insmod $MODULE_NAME_1.ko

echo "+ sleep 1"
sleep 1

set -x
echo -n "0123456789abcdefghijklmnopqrstu" > /dev/hd44780_platform_dev
set +x

#echo "+$SUDO rmmod $MODULE_NAME_1.ko"
#$SUDO rmmod $MODULE_NAME_1.ko

echo "+ lsmod | grep hd44780"
lsmod | grep hd44780

