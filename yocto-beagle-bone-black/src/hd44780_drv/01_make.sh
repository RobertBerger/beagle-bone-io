sudo apt-get install sparse

OLD_PATH=$PATH
PWD=`pwd`

source ../../env.sh

KERNELDIR=${CUSTOM_KERNELDIR}

make clean
make C=1

PATH=$OLD_PATH
cd $PWD
