/*
 * Copyright Robert Berger - Reliable Embedded Systems, 2016
@*/

/*
#define pr_fmt(fmt) KBUILD_MODNAME ": " fmt
*/

#define pr_fmt(fmt) KBUILD_MODNAME " %s:%d: " fmt, __func__, __LINE__

#include <linux/module.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fs.h>		/* file_operations */
#include <linux/uaccess.h>	/* copy_(to,from)_user */
#include <linux/slab.h>		/* kmalloc */
#include <linux/miscdevice.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/of_platform.h>
#include <linux/delay.h>
#include "hd44780.h"

#define DRV_NAME "hd44780_platform_drv"
#define DEV_NAME "hd44780_platform_dev"
#define KBUF_SIZE (size_t)(10*PAGE_SIZE)

/*
pins[0].pin - HD44780_E
pins[1].pin - HD44780_RS
pins[2].pin - HD44780_DB4
pins[3].pin - HD44780_DB5
pins[4].pin - HD44780_DB6
pins[5].pin - HD44780_DB7
*/

/* Macros for setting control lines */
#define HD_E_LOW   gpio_set_value(pins[0].pin, 0);
#define HD_E_HIGH  gpio_set_value(pins[0].pin, 1);

#define HD_RS_LOW  gpio_set_value(pins[1].pin, 0);
#define HD_RS_HIGH gpio_set_value(pins[1].pin, 1);

/* more Macros */
#define HD44780_DDRAM_2ND_LINE_OFFSET (0x40)
#define UDELAY_1   (1)
#define UDELAY_50  (50)
#define UDELAY_120 (120)

#define MSLEEP_20  (20)

/* Write a nibble to the display */
static void WriteNibble(unsigned int val)
{
	gpio_set_value(pins[2].pin, (val & 0x1));      /* db4 */
	gpio_set_value(pins[3].pin, (val & 0x2) >> 1); /* db5 */
	gpio_set_value(pins[4].pin, (val & 0x4) >> 2); /* db6 */
	gpio_set_value(pins[5].pin, (val & 0x8) >> 3); /* db7 */

	udelay(UDELAY_1);
	HD_E_LOW;
	udelay(UDELAY_1);		/* data setup time */
	HD_E_HIGH;
	udelay(UDELAY_1);		/* data hold time */
}

/* Write char data to display */
static void WriteData(char c)
{
	udelay(UDELAY_1);
	HD_RS_HIGH;
	udelay(UDELAY_1);
	WriteNibble((c >> 4) & 0xf);
	WriteNibble(c & 0xf);
	udelay(UDELAY_50);
}

/* Send command code to the display */
static void WriteCommand(char c)
{
	udelay(UDELAY_1);
	HD_RS_LOW;
	udelay(UDELAY_1);
	WriteNibble((c >> 4) & 0xf);
	WriteNibble(c & 0xf);
	udelay(UDELAY_50);
}

static void hd44780_set_ddram_address(char addr)
{
	WriteCommand((0x80)|(addr));
        udelay(UDELAY_120);
}

static void hd44780_cursor_line_2(void)
{
	hd44780_set_ddram_address(HD44780_DDRAM_2ND_LINE_OFFSET);
}

static int hd44780_drv_open(struct inode *inode, struct file *file)
{
	char *kbuf = kmalloc(KBUF_SIZE, GFP_KERNEL);
	file->private_data = kbuf;

        pr_info(" attempting to init LCD\n");

        /* Clear Screen */
        WriteCommand(0x01);
        udelay(UDELAY_50);

        /* Home */
        WriteCommand(0x02);
        udelay(UDELAY_50);

	pr_info(" attempting to open device: %s:\n", DEV_NAME);
	pr_info(" MAJOR number = %d, MINOR number = %d\n",
		imajor(inode), iminor(inode));
	pr_info(" successfully open  device: %s:\n\n", DEV_NAME);
	pr_info("ref=%d\n", module_refcount(THIS_MODULE));
	if (module_refcount(THIS_MODULE) == 1)
		pr_info("First time open: Allocate resources\n");
	return 0;
}

static int hd44780_drv_release(struct inode *inode, struct file *file)
{
	char *kbuf = file->private_data;
	kfree(kbuf);
	pr_info(" CLOSING device: %s:\n\n", DEV_NAME);
	pr_info("ref=%d\n", module_refcount(THIS_MODULE));
	if (module_refcount(THIS_MODULE) == 1)
		pr_info("Last time close: Deallocate resources\n");

	return 0;
}

/* Called when writing to the device file. */

static ssize_t hd44780_write(struct file *file, const char __user *buf,
			     size_t count, loff_t *ppos)
{
	int i;
	int nbytes, maxbytes, bytes_to_do;
	char *kbuf = file->private_data;
	maxbytes = KBUF_SIZE - *ppos;
	bytes_to_do = maxbytes > count ? count : maxbytes;
	pr_info("bytes_to_do %d\n", bytes_to_do);
	if (bytes_to_do == 0)
		pr_info("Reached end of the device on a write");

	nbytes = bytes_to_do - copy_from_user(kbuf + *ppos, buf, bytes_to_do);
	*ppos += nbytes;
	/* now we should have in the kernel buffer what to write */
	/* @@@ we might be off by one here */
	for (i = 0; i < nbytes; i++) {
		pr_info("kbuf[%d], %c 0x%x\n", i, kbuf[i], kbuf[i]);
		/* automatically move to 2nd line */
                if (i==16) {
			hd44780_cursor_line_2();
		}
		/* replace e.g. 0xa with space */
                if (kbuf[i]==0xa) {
			kbuf[i]=' ';
		}
		/* ignore 0x9 */
                if (kbuf[i]!=0x9) {
			WriteData(kbuf[i]);
                }
	}

	pr_info("\nLeaving the   WRITE function, nbytes=%d, pos=%d\n", nbytes,
		(int)*ppos);

	return nbytes;
}

static const struct file_operations hd44780_drv_fops = {
	.owner = THIS_MODULE,
	.write = hd44780_write,
	.open = hd44780_drv_open,
	.release = hd44780_drv_release
};

static struct miscdevice hd44780_misc_device = {
	.minor = MISC_DYNAMIC_MINOR,
	.name = DEV_NAME,
	.fops = &hd44780_drv_fops,
};

static struct platform_device *hd44780_platform_device;

static struct of_device_id hd44780_of_match[] = {
	{.compatible = "gpio-hd44780",},
	{}
};

MODULE_DEVICE_TABLE(of, hd44780_of_match);

/* return any acquired pins. */
static void FreePins(void)
{
	int i;
	for (i = 0; i < ngpios_fdt; i++) {
		if (pins[i].result == 0) {
			gpio_free(pins[i].pin);
			/* defensive programming - avoid multiple free. */
			pins[i].result = -1;
		}
	}
}

static int __init hd44780_platform_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *node;
	int ret, i;
	int got_pins = 1;

	/* --> it's called twize - protection */
	static int first_time = 1;

	if (first_time == 0)
		return 0;

	first_time = 0;
	/* <-- it's called twize - protection */

	/* --> register misc char device */
	ret = misc_register(&hd44780_misc_device);
	if (ret != 0) {
		pr_err("cannot register miscdev\n");
		goto out;
	}
	pr_info("initialized.\n");
	/* <-- register misc char device */

	node = dev->of_node;

	if (node) {
		pr_err("good\n");

		/* --> read rs gpio from fdt */

		ngpios_fdt = of_gpio_named_count(node, "e");

		if (ngpios_fdt != 1) {
			dev_err(dev, "e not defined in fdt\n");
			return -EINVAL;
		}
		pins[0].pin = of_get_named_gpio(node, "e", 0);

		/* <-- read rs gpio from fdt */

		/* --> read rw gpio from fdt */

		ngpios_fdt += of_gpio_named_count(node, "rs");

		if (ngpios_fdt != 2) {
			dev_err(dev, "rw not defined in fdt\n");
			return -EINVAL;
		}
		pins[1].pin = of_get_named_gpio(node, "rs", 0);

		/* <-- read rw gpio from fdt */

		/* --> read db4 gpio from fdt */

		ngpios_fdt += of_gpio_named_count(node, "db4");

		if (ngpios_fdt != 3) {
			dev_err(dev, "db4 not defined in fdt\n");
			return -EINVAL;
		}
		pins[2].pin = of_get_named_gpio(node, "db4", 0);

		/* <-- read db4 gpio from fdt */

		/* --> read db5 gpio from fdt */

		ngpios_fdt += of_gpio_named_count(node, "db5");

		if (ngpios_fdt != 4) {
			dev_err(dev, "db5 not defined in fdt\n");
			return -EINVAL;
		}
		pins[3].pin = of_get_named_gpio(node, "db5", 0);

		/* <-- read db5 gpio from fdt */

		/* --> read db6 gpio from fdt */

		ngpios_fdt += of_gpio_named_count(node, "db6");

		if (ngpios_fdt != 5) {
			dev_err(dev, "db6 not defined in fdt\n");
			return -EINVAL;
		}
		pins[4].pin = of_get_named_gpio(node, "db6", 0);

		/* <-- read db6 gpio from fdt */

		/* --> read db7 gpio from fdt */

		ngpios_fdt += of_gpio_named_count(node, "db7");

		if (ngpios_fdt != 6) {
			dev_err(dev, "db7 not defined in fdt\n");
			return -EINVAL;
		}
		pins[5].pin = of_get_named_gpio(node, "db7", 0);

		/* <-- read db7 gpio from fdt */

		/* Request pins */
		for (i = 0; i < ngpios_fdt; i++) {
			if (gpio_is_valid(pins[i].pin)) {
				pins[i].result =
				    gpio_request(pins[i].pin, pins[i].name);
				pr_info
				    ("pins[%d].pin,.name,.result: %d %s %d\n",
				     i, pins[i].pin, pins[i].name,
				     pins[i].result);
				if (pins[i].result != 0)
					got_pins = 0;
			} else {
				pr_info("gpio is invalid\n");
				got_pins = 0;
			}
		}

		/* On any failures,
		   return any pins we managed to get and quit.
		 */
		if (!got_pins) {
			FreePins();
			pr_warn("Could not get pins\n");
			return -EBUSY;
		}

		/* Set port direction.
		   We assume we can do this if we got the pins.
		   Set initial values to low (0v).
		 */
		for (i = 0; i < ngpios_fdt; i++)
			gpio_direction_output(pins[i].pin, 0);
#if 0
#define PIN_TO_TEST 0
                pr_info("--> Testing pins[%d].pin \n", PIN_TO_TEST);
                pr_info("low\n");
                gpio_set_value(pins[PIN_TO_TEST].pin, 0);
                msleep(1000);
                pr_info("high\n");
                gpio_set_value(pins[PIN_TO_TEST].pin, 1);
                msleep(1000);
                gpio_set_value(pins[PIN_TO_TEST].pin, 0);
                pr_info("low\n");
                pr_info("<-- Testing pins[%d].pin \n", PIN_TO_TEST);

                goto out;
#endif
		/* Power on and setup the display */
		WriteNibble(0x03);
		msleep(MSLEEP_20);
		WriteNibble(0x03);
		msleep(MSLEEP_20);
		WriteNibble(0x03);
		msleep(MSLEEP_20);
		WriteNibble(0x02);
		msleep(MSLEEP_20);

		/* Function set (4-bit interface, 2 lines, 5*7 Pixels) */
		WriteCommand(0x28);
		udelay(UDELAY_50);
		/* Make cursor invisible */
		WriteCommand(0x0c);
		udelay(UDELAY_50);
                /* Clear Screen */
		WriteCommand(0x01);
		udelay(UDELAY_50);
		/* Display Shift :OFF; Increment Address Counter */
		WriteCommand(0x06);
		/* Display Shift :ON; Increment Address Counter */
                /* WriteCommand(0x07); */
		udelay(UDELAY_50);

		/* hopefully here we are initialized */
		pr_info("initialized\n");
		return 0;

	} else {
		pr_err("bad\n");
 out:
		return ret;
	}
}

static int __exit hd44780_platform_remove(struct platform_device *pdev)
{
	/* --> it's called twize - protection */
	static int first_time = 1;

	if (first_time == 0)
		return 0;

	first_time = 0;
	/* <-- it's called twize - protection */

	pr_info("hd44780_platform_remove\n");
	FreePins();
	misc_deregister(&hd44780_misc_device);
	return 0;
}

static struct platform_driver hd44780_platform_driver = {
/*        .probe = hd44780_platform_probe, */
	.remove = __exit_p(hd44780_platform_remove),
	.driver = {
		   .name = DRV_NAME,
		   .owner = THIS_MODULE,
		   .of_match_table = of_match_ptr(hd44780_of_match),
		   },
/*        .id_table = hd44780_platform_id_table, */
};

/*module_platform_driver(hd44780_device_driver);*/

static int __init hd44780_init(void)
{
	int err;

	pr_info("hd44780_platform driver is initialising\n");

	hd44780_platform_device =
	    platform_device_register_simple(DRV_NAME, -1, NULL, 0);
	if (IS_ERR(hd44780_platform_device)) {
		pr_err("hd44780_platform_device failed to initialize\n");
		return PTR_ERR(hd44780_platform_device);
	}

	err =
	    platform_driver_probe(&hd44780_platform_driver,
				  hd44780_platform_probe);
	if (err) {
		pr_err("err: %d\n", err);
		goto unreg_platform_device;
	}
	pr_info("hd44780_platform driver initialized\n");
	return 0;

 unreg_platform_device:
	pr_err("hd44780_platform_driver_probe failed\n");
	platform_device_unregister(hd44780_platform_device);
	platform_driver_unregister(&hd44780_platform_driver);
	return err;
}

static void __exit hd44780_exit(void)
{
	pr_info("Unloading hd44780 platform device\n");
	platform_device_unregister(hd44780_platform_device);
	platform_driver_unregister(&hd44780_platform_driver);
	pr_info("platform device and driver unloaded\n");
}

module_init(hd44780_init);
module_exit(hd44780_exit);

MODULE_AUTHOR("Robert Berger");
MODULE_DESCRIPTION("hd44780 gpio platform driver");
MODULE_LICENSE("GPL v2");
