#source ../env.sh
#MODULE_NAME_1=lab1_platform
MODULE_NAME_1="hd44780_drv"
#DEVICE_NAME=mycdrv
#USER_SPACE_READ=lab1_read
#USER_SPACE_WRITE=lab1_write

if [ "$(id -u)" == "0" ]; then
  unset SUDO
else
  SUDO=sudo 
fi

echo "+ modinfo $MODULE_NAME_1.ko"
modinfo $MODULE_NAME_1.ko

echo "+$SUDO rmmod $MODULE_NAME_1.ko"
$SUDO rmmod $MODULE_NAME_1.ko

#plus_echo "$SUDO rm -f /dev/$DEVICE_NAME"
#$SUDO rm -f /dev/$DEVICE_NAME

echo "+ $SUDO insmod $MODULE_NAME_1.ko"
$SUDO insmod $MODULE_NAME_1.ko

echo "+ sleep 1"
sleep 1

set -x
ls -la /sys/bus/platform/drivers/ | grep hd4470
ls -la /sys/bus/platform/drivers/hd44780_platform_drv/
set +x

echo "+$SUDO rmmod $MODULE_NAME_1.ko"
$SUDO rmmod $MODULE_NAME_1.ko

echo "+ lsmod | grep $MODULE_NAME_1"
lsmod | grep $MODULE_NAME_1

