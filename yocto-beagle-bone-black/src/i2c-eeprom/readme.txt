http://docs.cubieboard.org/tutorials/cb1/development/access_at24c_eeprom_via_i2c
----------------

fdt:

--> /dev/i2c-0

&i2c0 {
 pinctrl-names = "default";
 pinctrl-0 = <&i2c0_pins>;

 status = "okay";
 clock-frequency = <400000>;

 tps: tps@24 {
  reg = <0x24>;
 };

 baseboard_eeprom: baseboard_eeprom@50 {
  compatible = "at,24c256";
  reg = <0x50>;
 };
};

i2cdetect -y -r 0

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- UU -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- UU -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: UU -- -- -- -- -- -- -- -- -- -- -- -- -- -- --              <----
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: UU -- -- -- -- -- -- --                         

<-- /dev/i2c-0

--> /dev/i2c-2

&i2c2 {
 status = "okay";
 pinctrl-names = "default";
 pinctrl-0 = <&i2c2_pins>;

 clock-frequency = <100000>;

 cape_eeprom0: cape_eeprom0@54 {
  compatible = "at,24c256";
  reg = <0x54>;
 };

 cape_eeprom1: cape_eeprom1@55 {
  compatible = "at,24c256";
  reg = <0x55>;
 };

 cape_eeprom2: cape_eeprom2@56 {
  compatible = "at,24c256";
  reg = <0x56>;
 };

 cape_eeprom3: cape_eeprom3@57 {
  compatible = "at,24c256";
  reg = <0x57>;
 };
};

---

eeprom not connected:

i2cdetect -y -r 2

     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- UU UU UU UU -- -- -- -- -- -- -- --    <----
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         



Interpreting the Output
Each cell in the output table will contain one of the following symbols:

    "--". The address was probed but no chip answered.
    "UU". Probing was skipped, because this address is currently in use by a driver. This strongly suggests that there is a chip at this address.
    An address number in hexadecimal, e.g. "2d" or "4e". A chip was found at this address.

<-- /dev/i2c-0

-----

root@beaglebone:~# dmesg | grep EEPROM
[    3.146963] at24 0-0050: 32768 byte 24c256 EEPROM, writable, 1 bytes/write
[    3.154251] at24 2-0054: 32768 byte 24c256 EEPROM, writable, 1 bytes/write
[    3.161499] at24 2-0055: 32768 byte 24c256 EEPROM, writable, 1 bytes/write
[    3.168734] at24 2-0056: 32768 byte 24c256 EEPROM, writable, 1 bytes/write
[    3.175988] at24 2-0057: 32768 byte 24c256 EEPROM, writable, 1 bytes/write

---

cat /sys/bus/i2c/devices/2-0054/eeprom                  
cat: /sys/bus/i2c/devices/2-0054/eeprom: Connection timed out

---

eeprom connected:

---

A2=VCC
A1=GND
A0=GND

address 0x54

---

root@beaglebone:~#  ls /sys/bus/i2c/devices/
0-0024  0-0034  0-0050  0-0070  2-0054  2-0055  2-0056  2-0057  i2c-0  i2c-2
root@beaglebone:~#  ls /sys/bus/i2c/devices/ | grep 2-0054
2-0054
root@beaglebone:~# 

ls /sys/bus/i2c/devices/2-0054/   

echo "This is a text to be stored in our EEPROM" > /sys/bus/i2c/devices/2-0054/eeprom

cat /sys/bus/i2c/devices/2-0054/eeprom | hexdump -C
00000000  54 68 69 73 20 69 73 20  61 20 74 65 78 74 20 74  |This is a text t|
00000010  6f 20 62 65 20 73 74 6f  72 65 64 20 69 6e 20 6f  |o be stored in o|
00000020  75 72 20 45 45 50 52 4f  4d 0a ff ff ff ff ff ff  |ur EEPROM.......|
00000030  ff ff ff ff ff ff ff ff  ff ff ff ff ff ff ff ff  |................|
*
00008000
root@beaglebone:~# 

--------

./at24c256_i2c.out -d /dev/i2c-2 -a 0x54 -o 100 -w "This is a test!"
i2c device file:/dev/i2c-2
slave_address:0x54
R/W offset:0x64
try to write: This is a test! (15 bytes)!

./at24c256_i2c.out -d /dev/i2c-2 -a 0x54 -o 100 -r 15
i2c device file:/dev/i2c-2
slave_address:0x54
R/W offset:0x64
read from at24c256 eeprom: This is a test! (15 bytes)

./at24c256_i2c.out -d /dev/i2c-2 -a 0x54 -o 0 -r 41 
i2c device file:/dev/i2c-2
slave_address:0x54
R/W offset:0x0
read from at24c256 eeprom: This is a text to be stored in our EEPROM (41 bytes)

./at24c256_i2c.out -d /dev/i2c-2 -a 0x54 -o 0 -r 41 
i2c device file:/dev/i2c-2
slave_address:0x54
R/W offset:0x0
read from at24c256 eeprom: This is a text to be stored in our EEPROM (41 bytes)

at24c256_i2c.out -o 0 -r 41
i2c device file:/dev/i2c-2
slave_address:0x54
R/W offset:0x0
read from at24c256 eeprom: This is a text to be stored in our EEPROM (41 bytes)

./at24c256_i2c.out -d /dev/i2c-0 -a 0x50 -o 1 -r 27
i2c device file:/dev/i2c-0
slave_address:0x50
R/W offset:0x1
read from at24c256 eeprom: U3335BNLT0A6A0314BBBK0395 (27 bytes)



----

with nunchuck connected:

i2cdetect -y -r 2
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- 52 -- UU UU UU UU -- -- -- -- -- -- -- --     <-- 52
60: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
70: -- -- -- -- -- -- -- --                         

in an endless loop:

while i2cdetect -y -r 2; do i2cdetect -y -r 2; echo -n .; done

---------
