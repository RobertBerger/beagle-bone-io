HERE=`pwd`

source ../../env.sh

rm -rf include
mkdir -p include/linux/iio

cp ${CUSTOM_KERNELDIR}/include/linux/iio/events.h include/linux/iio
cp  ${CUSTOM_KERNELDIR}/include/linux/iio/types.h include/linux/iio

make clean
make

make clean -f Makefile-lsiio
make -f Makefile-lsiio

cd ${HERE}
