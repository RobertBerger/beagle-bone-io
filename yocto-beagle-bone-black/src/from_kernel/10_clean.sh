OLD_PATH=$PATH
PWD=`pwd`

source ../../env.sh

make clean
make -f Makefile-lsiio clean

rm -rf include

PATH=$OLD_PATH
cd $PWD

