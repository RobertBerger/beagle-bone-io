if [ $# -eq 0 ] ; then
    echo "00_indent.sh <file you want to check>"
    exit 0
fi

INDENT="indent -nbad -bap -nbc -bbo -hnl -br -brs -c33 -cd33 -ncdb -ce -ci4 -cli0 -d0 -di1 -nfc1 -i8 -ip0 -l80 -lp -npcs -nprs -npsl -sai -saf -saw -ncs -nsc -sob -nfca -cp33 -ss -ts8 -il1"
echo "+ ${INDENT} $1"
${INDENT} $1

