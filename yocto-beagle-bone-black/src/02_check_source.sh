if [ $# -eq 0 ] ; then
    echo "02_check_source.sh <file you want to check>"
    exit 0
fi

#DEVKIT=armv5te
source ../../env.sh
echo "+ ${CUSTOM_KERNELDIR}/scripts/checkpatch.pl --file --terse $1"
${CUSTOM_KERNELDIR}/scripts/checkpatch.pl --file --terse $1
