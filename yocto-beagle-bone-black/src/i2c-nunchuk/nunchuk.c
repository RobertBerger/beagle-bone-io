/*
 * Copyright (c) 2012 Janick Bergeron
 * All Rights Reserved - from here: 
 * http://sourceforge.net/p/bonelib/blog/2012/12/connecting-a-wii-nunchuck-to-the-beaglebone/
 *
 * The program below will try to initialize the Nunchuck on I2C2 port of the BeagleBone.
 *
 * Note that using an I²C port in Linux is easy as doing an open, an ioctl to set the peripheral 
 * address (beacuse on the same I²C bus you may connect more than one peripheral) and then 
 * normal read/write data. In this example I write one byte at a time.
 * 
 * The Nunchuck requires two bytes to start emitting data packets, and a single zero-valued 
 * byte to prepare next data packet. Packets are 6-bytes wide with a weak "encryption" 
 * (that "XOR 0x17 - 0x17") and a somewhat weird data packing:
 * -- two bytes for the joystick status (x axis and y axis, "center" value is about 128)
 * -- three bytes for accelerometer values; the x/y/z values are actually 10 bits wide; here we have the 8 most significant bits;
 * -- one byte for the two keys ("C" is the small key, "Z" is the large key) and for the extra 2 bits for x/y/z accelerometers values.
 *
 * Original code: http://www.alfonsomartone.itb.it/mzscbb.html
 * with bug fixes and display style from http://elinux.org/BeagleBoard_Trainer_Nunchuk
 *
 * It turns out, inserting a delay between the WRITE and READ commands is essential 
 * to let the device sample the sensor data. Through trial and error, this delay must be 100 usec or more.
 * I simplified the code structure (borrowing from the first code I found) and added a moving average 
 * for the acceleration values to make them easier to read/observe.
 *
 *
 *   Licensed under the Apache License, Version 2.0 (the
 *   "License"); you may not use this file except in
 *   compliance with the License.  You may obtain a copy of
 *   the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in
 *   writing, software distributed under the License is
 *   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *   CONDITIONS OF ANY KIND, either express or implied.  See
 *   the License for the specific language governing
 *   permissions and limitations under the License.
 *
 */

#define PORT "/dev/i2c-1"
/* char PORT[] = "/dev/i2c-2"; */
#define ADDR  0x52		/* wii nunchuck address: 0x52 */
/*char ADDR[] = "0x52";*/

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>

int main()
{
	/* Open port, ioctl address, init nunchuck to unencrypted mode */
	int fd = open(PORT, O_RDWR);
	/* loop: read 6 bytes, parse, print */
	unsigned char buf[6];
	unsigned int avg[3] = { 0, 0, 0 };
	int n, a;

	if (fd < 0) {
		fprintf(stderr, "ERROR: Cannot open \"%s\": %s\n", PORT,
			strerror(errno));
		exit(-1);
	}

	if (ioctl(fd, I2C_SLAVE, ADDR) < 0) {
		fprintf(stderr,
			"ERROR: Cannot access device 0x%02x on \"%s\": %s\n",
			ADDR, PORT, strerror(errno));
		exit(-1);
	}

	if (write(fd, "\xF0\x55", 2) != 2) {
		fprintf(stderr,
			"ERROR: Cannot initialize device 0x%02x@0xF0 on \"%s\": %s\n",
			ADDR, PORT, strerror(errno));
		exit(-1);
	}
	if (write(fd, "\xFB\x00", 2) != 2) {
		fprintf(stderr,
			"ERROR: Cannot initialize device 0x%02x@0xF0 on \"%s\": %s\n",
			ADDR, PORT, strerror(errno));
		exit(-1);
	}
	/* loop: read 6 bytes, parse, print */
/*
	unsigned char buf[6];
	unsigned int avg[3] = { 0, 0, 0 };
*/
	for (;;) {
		if (write(fd, "\x00\x00", 2) != 2) {
			fprintf(stderr,
				"ERROR: Cannot prime device 0x%02x@0x00 on \"%s\": %s\n",
				ADDR, PORT, strerror(errno));
			exit(-1);
		}
		/* delay for adjusting reporting speed. Bad data without. */
		usleep(200);

		n = read(fd, buf, 6);
		if (n != 6) {
			fprintf(stderr,
				"ERROR: Cannot read (%0d) from device 0x%02x@0x00 on \"%s\": %s\n",
				n, ADDR, PORT, strerror(errno));
			exit(-1);
		}

		printf("jx:%-3d  jy:%-3d  ", buf[0], buf[1]);	/* joystick x/y values */
		printf("%c %c  ", buf[5] & 1 ? '.' : 'Z', buf[5] & 2 ? '.' : 'C');	/* keys Z/C */
		for (n = 1; n <= 3; n++) {
			a = (buf[n + 1] << 2) + ((buf[5] >> (n + n)) & 3);	/* accelerometer x/y/z values */
			avg[n - 1] = (avg[n - 1] * 10 + a) / 11;	/* Moving average */
			printf("a%c:%-4d ", 'w' + n, avg[n - 1]);
		}

		printf("\r");
	}
}
